#!/bin/bash

set -e

. ./vvfe-lib.sh

TMP="$(mktemp -d --tmpdir vvfe.XXXXXX)"

dispatch "$1" "$TMP"
verify "$1" "$TMP"

echo "Output data left in $TMP"
