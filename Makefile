build:
	dune build -p vvfe-platform,vvfe-platform-native,vvfe-platform-js,vvfe-lib,vvfe-tool,vvfe-web

all: build bench check

debug:
	dune build

bench: build
	_build/install/default/bin/vvfe-tool bench 10000

check: build
	VVFE_OPERATION=8001 VVFE_TOUR=1 ./run.sh testdata/1
	VVFE_OPERATION=8001 VVFE_TOUR=2 ./run.sh testdata/2

clean:
	dune clean
