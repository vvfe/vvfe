#!/bin/bash

TOOL="_build/install/default/bin/vvfe-tool"
SHARE="_build/install/default/share/vvfe-web"

if ! [ -f "$TOOL" ]; then
    make
fi

compute_fingerprints () {
    echo "-----> Computing fingerprints <-----"
    ls -1d "$1"/*/* | grep -E '/[0-9]+$' | xargs -d '\n' parallel "$TOOL" compute-fingerprints -- &&
    ls -1d "$1"/* | grep -E '/[0-9]+$' | xargs -d '\n' parallel "$TOOL" compute-fingerprints --
}

dispatch () {
    \time "$TOOL" dispatch "$1" "$2"
}

compute_encrypted_tallies () {
    echo "-----> Computing encrypted tallies <-----"
    ls -1d "$1"/*/* | grep -E '/[0-9]+$' | xargs -d '\n' parallel "$TOOL" compute-encrypted-tally -- &&
    ls -1d "$1"/* | grep -E '/[0-9]+$' | xargs -d '\n' parallel "$TOOL" compute-encrypted-tally --
}

compute_partial_decryptions () {
    echo "-----> Computing partial decryptions <-----"
    ls -1d "$1"/* "$1"/*/* | grep -E '/[0-9]+$' | xargs -d '\n' parallel "$TOOL" compute-partial-decryptions "" --
}

compute_results () {
    echo "-----> Computing results <-----"
    PDS="$(find "$2" -name partial_decryptions.jsons)"
    if [ -n "$PDS" ]; then
        echo -n "$PDS" | sed 's@/partial_decryptions.jsons$@@' | xargs -d '\n' parallel "$TOOL" compute-result "$1" -- &&
        "$TOOL" dispatch-results "$2" "$2/export" &&
        "$TOOL" compute-election-map "$1" > "$2/election_map.json" &&
        "$TOOL" populate-web-iverif "$2/election_map.json" "$SHARE" "$1"/*.pem "$2/export"
    else
        echo "No partial decryptions found"
    fi
}

verify () {
    compute_encrypted_tallies "$2" &&
    compute_fingerprints "$2" &&
    compute_results "$1" "$2"
}

random () {
    export VVFE_CACHET_PRIVKEY=e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855
    # The above is the sha256 of the empty string
    # The associated public key is 131e2d69f16e0594173bad74b8bd2fefa6cd9d8a240fb9be58128549874ec3a9%9b42a6c192859ea7bee6214e8ff548479d3f047b500c571e2bb705f858bff80b
    SRC="$1"
    N="$2"
    TMP="$(mktemp -d --tmpdir vvfe.XXXXXX)"
    "$TOOL" generate-random-tree "" 16 4 "$N" "$SRC" "$TMP" &&
    compute_encrypted_tallies "$TMP" &&
    compute_fingerprints "$TMP" &&
    compute_partial_decryptions "$TMP" &&
    compute_results "$SRC" "$TMP" &&
    echo "Tree generated in $TMP"
}

random_per_et () {
    export VVFE_CACHET_PRIVKEY=e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855
    SRC="$1"
    N="$2"
    TMP="$(mktemp -d --tmpdir vvfe.XXXXXX)"
    "$TOOL" init-random-tree-full "" 16 4 "$SRC" "$TMP" &&
    ls -1d "$TMP"/*/* | grep -E '/[0-9]+$' | xargs -d '\n' parallel "$TOOL" generate-random-ballots "" "$N" -- &&
    compute_encrypted_tallies "$TMP" &&
    compute_fingerprints "$TMP" &&
    compute_partial_decryptions "$TMP" &&
    compute_results "$SRC" "$TMP" &&
    echo "Tree generated in $TMP"
}

generate_recepisse () {
    BALLOT="$1"
    ELECTION="$2"
    ETABLISSEMENT="$3"
    SRC="$4"
    echo -n "$BALLOT" | "$TOOL" generate-recepisse "$BALLOT" "$ELECTION" "$ETABLISSEMENT" "$SRC"
}

random_with_invalid_and_recepisses () {
    export VVFE_CACHET_PRIVKEY=e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855
    SRC="$1"
    N=100
    TMP="$(mktemp -d --tmpdir vvfe.XXXXXX)"
    "$TOOL" generate-random-tree "" 16 4 "$N" "$SRC" "$TMP" &&
    BALLOTS="$(ls "$TMP"/*/*/ballots.jsons | head -n 1)" &&
    echo "Injecting invalid ballot in $BALLOTS..." &&
    ELECTION="$(echo "$BALLOTS" | sed -r 's@^.*/([^/]+)/([^/]+)/ballots\.jsons@\1@')" &&
    ETABLISSEMENT="$(echo "$BALLOTS" | sed -r 's@^.*/([^/]+)/([^/]+)/ballots\.jsons@\2@')" &&
    BALLOT_VALID="$(head -n 1 "$BALLOTS")" &&
    BALLOT_INVALID="$(echo "$BALLOT_VALID" | sed 's/FIXME/BREAKME/g')" &&
    echo "$BALLOT_INVALID" >> "$BALLOTS" &&
    compute_encrypted_tallies "$TMP" &&
    compute_fingerprints "$TMP" &&
    compute_partial_decryptions "$TMP" &&
    compute_results "$SRC" "$TMP" &&
    generate_recepisse "$BALLOT_VALID" "$ELECTION" "$ETABLISSEMENT" "$SRC" > "$TMP/export/recepisse-vrai.txt" &&
    generate_recepisse "$BALLOT_INVALID" "$ELECTION" "$ETABLISSEMENT" "$SRC" > "$TMP/export/recepisse-nul.txt" &&
    generate_recepisse "" "$ELECTION" "$ETABLISSEMENT" "$SRC" > "$TMP/export/recepisse-faux.txt" &&
    echo "Tree generated in $TMP"
}
