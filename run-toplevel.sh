#!/bin/sh
export CAML_LD_LIBRARY_PATH=_build/install/default/lib/stublibs:$CAML_LD_LIBRARY_PATH
export OCAMLPATH=_build/install/default/lib:$OCAMLPATH
exec "$@"
