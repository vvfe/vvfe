(**************************************************************************)
(*                                  VVFE                                  *)
(*                                                                        *)
(*  Copyright © 2021-2022 Inria                                           *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU Affero General Public License as        *)
(*  published by the Free Software Foundation, either version 3 of the    *)
(*  License, or (at your option) any later version, with the additional   *)
(*  exemption that compiling, linking, and/or using OpenSSL is allowed.   *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful, but   *)
(*  WITHOUT ANY WARRANTY; without even the implied warranty of            *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *)
(*  Affero General Public License for more details.                       *)
(*                                                                        *)
(*  You should have received a copy of the GNU Affero General Public      *)
(*  License along with this program.  If not, see                         *)
(*  <http://www.gnu.org/licenses/>.                                       *)
(**************************************************************************)

open Platform

let map_and_concat_with_commas f xs =
  let n = Array.length xs in
  let res = Buffer.create (n * 1024) in
  for i = 0 to n-1 do
    Buffer.add_string res (f xs.(i));
    Buffer.add_char res ',';
  done;
  let size = Buffer.length res - 1 in
  if size > 0 then Buffer.sub res 0 size else ""

let check_modulo p x = Z.(compare x zero >= 0 && compare x p < 0)

let of_string_generic alphabet =
  let base = Z.of_int (String.length alphabet) in
  fun s ->
  let n = String.length s in
  let rec loop i accu =
    if i < n then
      loop (i + 1) Z.(accu * base + of_int (String.index alphabet s.[i]))
    else
      accu
  in
  loop 0 Z.zero

let to_string_generic alphabet =
  let base = Z.of_int (String.length alphabet) in
  fun x ->
  let buf = Buffer.create 64 in
  let rec loop x =
    if Z.(compare x zero = 0) then (
      let t = Buffer.contents buf in
      let n = String.length t in
      if n = 0 then "0" else String.init n (fun i -> t.[n - 1 - i])
    ) else (
      Buffer.add_char buf alphabet.[Z.(to_int (x mod base))];
      loop Z.(x / base)
    )
  in
  loop x

let base16_alphabet = "0123456789abcdef"
let of_string_base16 = of_string_generic base16_alphabet
let to_string_base16 = to_string_generic base16_alphabet

let base36_alphabet = "0123456789abcdefghijklmnopqrstuvwxyz"
let of_string_base36 = of_string_generic base36_alphabet
let to_string_base36 = to_string_generic base36_alphabet

let base256_alphabet = String.init 256 char_of_int
let of_string_base256 = of_string_generic base256_alphabet
let to_string_base256 = to_string_generic base256_alphabet
