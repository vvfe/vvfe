(**************************************************************************)
(*                                  VVFE                                  *)
(*                                                                        *)
(*  Copyright © 2012-2021 Inria                                           *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU Affero General Public License as        *)
(*  published by the Free Software Foundation, either version 3 of the    *)
(*  License, or (at your option) any later version, with the additional   *)
(*  exemption that compiling, linking, and/or using OpenSSL is allowed.   *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful, but   *)
(*  WITHOUT ANY WARRANTY; without even the implied warranty of            *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *)
(*  Affero General Public License for more details.                       *)
(*                                                                        *)
(*  You should have received a copy of the GNU Affero General Public      *)
(*  License along with this program.  If not, see                         *)
(*  <http://www.gnu.org/licenses/>.                                       *)
(**************************************************************************)

open Js_of_ocaml

let vvfe = Js.Unsafe.pure_js_expr "vvfe"

let debug x = Firebug.console##log (Js.string x)

module Sjcl = struct
  open Js

  type bits

  class type codec =
    object
      method fromBits : bits -> js_string t meth
      method toBits : js_string t -> bits meth
    end

  class type codecs =
    object
      method hex : codec t readonly_prop
      method utf8String : codec t readonly_prop
      method base64 : codec t readonly_prop
    end

  class type hash =
    object
      method hash : js_string t -> bits meth
    end

  class type hashes =
    object
      method sha256 : hash t readonly_prop
    end

  class type sjcl =
    object
      method codec : codecs t readonly_prop
      method hash : hashes t readonly_prop
    end

  let sjcl : sjcl t = vvfe##.sjcl

  let hex = sjcl##.codec##.hex
  let utf8String = sjcl##.codec##.utf8String
  let base64 = sjcl##.codec##.base64
  let sha256 = sjcl##.hash##.sha256
end

let hex_fromBits x =
  Sjcl.hex##fromBits x |> Js.to_string

let utf8String_fromBits x =
  Sjcl.utf8String##fromBits x |> Js.to_string

let utf8String_toBits x =
  Sjcl.utf8String##toBits (Js.string x)

let decode_base64 x =
  Sjcl.base64##toBits (Js.string x) |> utf8String_fromBits

let encode_base64 x =
  Sjcl.base64##fromBits (utf8String_toBits x) |> Js.to_string

let sha256 x =
  Sjcl.sha256##hash (Js.string x)

let sha256_hex x =
  hex_fromBits (sha256 x)

let sha256_b64 x =
  Sjcl.base64##fromBits (sha256 x) |> Js.to_string

module BigIntCompat = struct
  open Js
  type bigint

  class type lib =
    object
      method _ZERO : bigint readonly_prop
      method _ONE : bigint readonly_prop
      method ofInt : int -> bigint meth
      method ofString : js_string t -> bigint meth
      method ofHex : js_string t -> bigint meth
      method add : bigint -> bigint -> bigint meth
      method subtract : bigint -> bigint -> bigint meth
      method multiply : bigint -> bigint -> bigint meth
      method divide : bigint -> bigint -> bigint meth
      method _mod : bigint -> bigint -> bigint meth
      method toInt : bigint -> int meth
      method toString : bigint -> js_string t meth
      method compare : bigint -> bigint -> int meth
      method modPow : bigint -> bigint -> bigint -> bigint meth
      method modInverse : bigint -> bigint -> bigint meth
      method shiftLeft : bigint -> int -> bigint meth
      method shiftRight : bigint -> int -> bigint meth
      method _and : bigint -> bigint -> bigint meth
    end

  let lib : lib t = vvfe##._BigIntCompat
end

module Z = struct
  open BigIntCompat
  type t = bigint

  let zero = lib##._ZERO
  let one = lib##._ONE

  let of_hex x = lib##ofHex (Js.string x)
  let of_string x = lib##ofString (Js.string x)
  let of_int x = lib##ofInt x
  let ( + ) x y = lib##add x y
  let ( - ) x y = lib##subtract x y
  let ( * ) x y = lib##multiply x y
  let ( / ) x y = lib##divide x y
  let ( mod ) x y = lib##_mod x y

  let to_int x = lib##toInt x
  let to_string x = lib##toString x |> Js.to_string
  let compare x y = lib##compare x y
  let ( =% ) x y = compare x y = 0
  let powm x y m = lib##modPow x y m
  let invert x m = lib##modInverse x m

  let erem x y =
    let r = x mod y in
    if compare r zero < 0 then r + y else r

  let shift_left x n = lib##shiftLeft x n
  let shift_right x n = lib##shiftRight x n
  let logand x y = lib##_and x y
  let logor _ _ = failwith "not implemented: logor"
  let logxor _ _ = failwith "not implemented: logxor"

  let hash_modulus = of_int 1073741789 (* previous_prime(2^30) *)
  let hash_to_int x = to_int (erem x hash_modulus)
end
