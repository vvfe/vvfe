(**************************************************************************)
(*                                  VVFE                                  *)
(*                                                                        *)
(*  Copyright © 2021-2022 Inria                                           *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU Affero General Public License as        *)
(*  published by the Free Software Foundation, either version 3 of the    *)
(*  License, or (at your option) any later version, with the additional   *)
(*  exemption that compiling, linking, and/or using OpenSSL is allowed.   *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful, but   *)
(*  WITHOUT ANY WARRANTY; without even the implied warranty of            *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *)
(*  Affero General Public License for more details.                       *)
(*                                                                        *)
(*  You should have received a copy of the GNU Affero General Public      *)
(*  License along with this program.  If not, see                         *)
(*  <http://www.gnu.org/licenses/>.                                       *)
(**************************************************************************)

open Platform

val map_and_concat_with_commas : ('a -> string) -> 'a array -> string
val check_modulo : Z.t -> Z.t -> bool

val of_string_generic : string -> string -> Z.t
val to_string_generic : string -> Z.t -> string

val of_string_base16 : string -> Z.t
val to_string_base16 : Z.t -> string

val of_string_base36 : string -> Z.t
val to_string_base36 : Z.t -> string

val of_string_base256 : string -> Z.t
val to_string_base256 : Z.t -> string
