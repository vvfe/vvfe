(**************************************************************************)
(*                                  VVFE                                  *)
(*                                                                        *)
(*  Copyright © 2012-2021 Inria                                           *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU Affero General Public License as        *)
(*  published by the Free Software Foundation, either version 3 of the    *)
(*  License, or (at your option) any later version, with the additional   *)
(*  exemption that compiling, linking, and/or using OpenSSL is allowed.   *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful, but   *)
(*  WITHOUT ANY WARRANTY; without even the implied warranty of            *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *)
(*  Affero General Public License for more details.                       *)
(*                                                                        *)
(*  You should have received a copy of the GNU Affero General Public      *)
(*  License along with this program.  If not, see                         *)
(*  <http://www.gnu.org/licenses/>.                                       *)
(**************************************************************************)

let debug x = prerr_endline x

let decode_base64 x =
  let open Cryptokit in
  x |> transform_string (Base64.decode ())

let encode_base64 x =
  let open Cryptokit in
  x |> transform_string (Base64.encode_compact_pad ())

let sha256_hex x = Cryptokit.(x |>
  hash_string (Hash.sha256 ()) |>
  transform_string (Hexa.encode ())
)

let sha256_b64 x = Cryptokit.(x |>
  hash_string (Hash.sha256 ()) |>
  transform_string (Base64.encode_compact_pad ())
)

module Z = struct
  include Z
  let of_hex x = of_string_base 16 x
  let ( =% ) = equal

  let powm x a m =
    if Z.compare a Z.zero = 0 then Z.one else powm_sec x a m
    (* Warning: no efforts have been made to be constant time in the
       rest of the code. *)

  let hash_to_int = Z.hash
end
