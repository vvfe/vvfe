(**************************************************************************)
(*                                  VVFE                                  *)
(*                                                                        *)
(*  Copyright © 2021-2022 Inria                                           *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU Affero General Public License as        *)
(*  published by the Free Software Foundation, either version 3 of the    *)
(*  License, or (at your option) any later version, with the additional   *)
(*  exemption that compiling, linking, and/or using OpenSSL is allowed.   *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful, but   *)
(*  WITHOUT ANY WARRANTY; without even the implied warranty of            *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *)
(*  Affero General Public License for more details.                       *)
(*                                                                        *)
(*  You should have received a copy of the GNU Affero General Public      *)
(*  License along with this program.  If not, see                         *)
(*  <http://www.gnu.org/licenses/>.                                       *)
(**************************************************************************)

open Platform
open Common

(** Ed25519 implementation using libsodium *)

let q = Z.(shift_left one 255 - of_int 19)
let l = Z.(shift_left one 252 + of_string "27742317777372353535851937790883648493")

module E = struct
  type scalar = bytes
  type point = bytes

  external bytes : unit -> int = "vvfe_sodium_ed25519_bytes" [@@noalloc]
  external scalarbytes : unit -> int = "vvfe_sodium_ed25519_scalarbytes" [@@noalloc]
  external is_valid_point : point -> int = "vvfe_sodium_ed25519_is_valid_point" [@@noalloc]
  external scalarmult : point -> scalar -> point -> int = "vvfe_sodium_ed25519_scalarmult" [@@noalloc]
  external scalarmult_base : point -> scalar -> int = "vvfe_sodium_ed25519_scalarmult_base" [@@noalloc]

  let bytes = bytes ()
  let scalarbytes = scalarbytes ()

  let create_point () = Bytes.create bytes
  let compare_points = Bytes.compare

  let z255 = Z.of_int 255

  let of_z_generic n z =
    let result = Bytes.create n in
    let rec loop i z =
      if i < n then (
        Bytes.set result i Z.(logand z z255 |> to_int |> char_of_int);
        loop (i + 1) Z.(shift_right z 8)
      ) else result
    in
    loop 0 z

  let scalar_of_number z =
    assert Z.(compare z l < 0);
    of_z_generic scalarbytes z

  let point_of_coordinates (x, y) =
    let open Z in
    let b = shift_left (logand x one) 255 in
    of_z_generic bytes (logxor y b)

  let number_of_scalar b =
    let rec loop i accu =
      if i >= 0 then
        loop (i - 1) Z.(logor (shift_left accu 8) (Bytes.get b i |> int_of_char |> of_int))
      else
        accu
    in
    loop (Bytes.length b - 1) Z.zero

  let mask255 = Z.(shift_left one 255 - one)

  let modsqrt =
    (* https://www.rieselprime.de/ziki/Modular_square_root *)
    let open Z in
    let five = of_int 5 and eight = of_int 8 in
    assert Z.(compare (q mod eight) five = 0);
    let exp = (q - five) / eight in
    fun a ->
    let v = powm (shift_left a 1) exp q in
    let i = erem (shift_left (a * v * v) 1) q in
    erem (a * v * (i - one)) q

  let coordinates_of_point =
    let open Z in
    let d = erem (zero - of_int 121665 * invert (of_int 121666) q) q in
    fun p ->
    let raw = number_of_scalar p in
    let y = logand raw mask255 in
    let y2 = erem (y * y) q in
    let x2 = erem ((y2 - one) * invert (d * y2 + one) q) q in
    let x = modsqrt x2 in
    assert (compare (erem (x * x) q) x2 = 0);
    let xsign = logand x one in
    let rsign = shift_right raw 255 in
    let x = if compare xsign rsign = 0 then x else erem (zero - x) q in
    (x, y)
end

module G = Ed25519_pure

type t =
  {
    mutable pure : G.t option;
    mutable nacl : E.point option;
  }

let get_as_pure p =
  match p.pure with
  | Some a -> a
  | None ->
     match p.nacl with
     | Some a ->
        let x, y = E.coordinates_of_point a in
        let b = G.of_coordinates (x, y) in
        p.pure <- Some b;
        b
     | None -> failwith "inconsistency in Ed25519_libsodium.get_as_pure"

let get_as_nacl p =
  match p.nacl with
  | Some a -> a
  | None ->
     match p.pure with
     | Some a ->
        let x, y = G.to_coordinates a in
        let b = E.point_of_coordinates (x, y) in
        p.nacl <- Some b;
        b
     | None -> failwith "inconsistency in Ed25519_libsodium.get_as_nacl"

let to_coordinates p =
  G.to_coordinates (get_as_pure p)

let of_coordinates (x, y) =
  {
    pure = Some (G.of_coordinates (x, y));
    nacl = None;
  }

let check p =
  match p.nacl, p.pure with
  | Some a, _ -> E.is_valid_point a = 1 || G.check (get_as_pure p)
  | _, Some a -> G.check a
  | None, None -> failwith "inconsistency in Ed25519_libsodium.check"

let one = of_coordinates Z.(zero, one)

let g =
  let r = E.create_point () in
  if E.scalarmult_base r (E.scalar_of_number Z.one) = 0 then
    {
      nacl = Some r;
      pure = None;
    }
  else
    invalid_arg "failure in computing Ed25519_libsodium.g"

let () = assert (check one)
let () = assert (check g)
let () = assert (G.compare (get_as_pure g) G.g = 0)

let ( *~ ) a b =
  {
    pure = Some G.(get_as_pure a *~ get_as_pure b);
    nacl = None;
  }

let ( **~ ) p n =
  let r = E.create_point () in
  if E.scalarmult r (E.scalar_of_number n) (get_as_nacl p) = 0 then
    {
      nacl = Some r;
      pure = None;
    }
  else
    {
      pure = Some G.(get_as_pure p **~ n);
      nacl = None;
    }

let compare a b =
  match a.pure, b.pure, a.nacl, b.nacl with
  | Some c, Some d, _, _ -> G.compare c d
  | _, _, Some c, Some d -> E.compare_points c d
  | _, _, Some c, _ -> E.compare_points c (get_as_nacl b)
  | _, _, _, Some d -> E.compare_points (get_as_nacl a) d
  | _, _, None, None -> G.compare (get_as_pure a) (get_as_pure b)

let ( =~ ) a b = compare a b = 0

let () = assert (g **~ Z.(l - one) *~ g =~ one)

let invert p =
  {
    pure = Some G.(invert (get_as_pure p));
    nacl = None;
  }

let () = assert (g *~ invert g =~ one)

let to_string p =
  G.to_string (get_as_pure p)

let of_string s =
  {
    pure = Some (G.of_string s);
    nacl = None;
  }

let hash prefix xs =
  let x = prefix ^ (map_and_concat_with_commas to_string xs) in
  let z = Z.of_hex (sha256_hex x) in
  Z.(z mod l)

let hash_to_int p =
  let x, y = to_coordinates p in
  Z.(hash_to_int (shift_left x 256 + y))

let description = "Ed25519"

let q = l
