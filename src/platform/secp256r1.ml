(**************************************************************************)
(*                                  VVFE                                  *)
(*                                                                        *)
(*  Copyright © 2021-2022 Inria                                           *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU Affero General Public License as        *)
(*  published by the Free Software Foundation, either version 3 of the    *)
(*  License, or (at your option) any later version, with the additional   *)
(*  exemption that compiling, linking, and/or using OpenSSL is allowed.   *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful, but   *)
(*  WITHOUT ANY WARRANTY; without even the implied warranty of            *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *)
(*  Affero General Public License for more details.                       *)
(*                                                                        *)
(*  You should have received a copy of the GNU Affero General Public      *)
(*  License along with this program.  If not, see                         *)
(*  <http://www.gnu.org/licenses/>.                                       *)
(**************************************************************************)

open Platform

(* https://neuromancer.sk/std/secg/secp256r1 *)

module P = struct
  let description = "secp256r1"
  let p = Z.of_hex "ffffffff00000001000000000000000000000000ffffffffffffffffffffffff"
  let a = Z.of_hex "ffffffff00000001000000000000000000000000fffffffffffffffffffffffc"
  let b = Z.of_hex "5ac635d8aa3a93e7b3ebbd55769886bc651d06b0cc53b0f63bce3c3e27d2604b"
  let g =
    (
      Z.of_hex "6b17d1f2e12c4247f8bce6e563a440f277037d812deb33a0f4a13945d898c296",
      Z.of_hex "4fe342e2fe1a7f9b8ee7eb4a7c0f9e162bce33576b315ececbb6406837bf51f5"
    )
  let q = Z.of_hex "ffffffff00000000ffffffffffffffffbce6faada7179e84f3b9cac2fc632551"
end

include Short_weierstrass.Make (P)
