(**************************************************************************)
(*                                  VVFE                                  *)
(*                                                                        *)
(*  Copyright © 2021-2022 Inria                                           *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU Affero General Public License as        *)
(*  published by the Free Software Foundation, either version 3 of the    *)
(*  License, or (at your option) any later version, with the additional   *)
(*  exemption that compiling, linking, and/or using OpenSSL is allowed.   *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful, but   *)
(*  WITHOUT ANY WARRANTY; without even the implied warranty of            *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *)
(*  Affero General Public License for more details.                       *)
(*                                                                        *)
(*  You should have received a copy of the GNU Affero General Public      *)
(*  License along with this program.  If not, see                         *)
(*  <http://www.gnu.org/licenses/>.                                       *)
(**************************************************************************)

open Platform
open Common

type number = Z.t

module type PARAMS = sig
  val description : string
  val a : number
  val b : number
  val p : number
  val q : number
  val g : number * number
end

module Make (P : PARAMS) = struct
  open P

  module F = struct
    let zero = Z.zero
    let one = Z.one
    let compare = Z.compare
    let reduce x = Z.erem x p
    let double a = reduce Z.(shift_left a 1)
    let ( + ) a b = reduce Z.(a + b)
    let ( * ) a b = reduce Z.(a * b)
    let ( - ) a b = reduce Z.(a - b)
    let square a = a * a
    let invert a = Z.invert a p
  end

  type t = number * number * number

  let check (x, y, z) =
    let lhs = F.(z * y * y) in
    let rhs = F.(x * x * x + a * z * z * x + b * z * z * z) in
    F.compare lhs rhs = 0

  let one = F.(zero, one, zero)
  let is_one (_, _, z) = F.(compare z zero) = 0

  let of_coordinates = function
    | None -> one
    | Some (x, y) -> (x, y, F.one)

  let to_coordinates ((x, y, z) as p) =
    if is_one p then (
      None
    ) else (
      let invz = F.invert z in
      Some F.(x * invz, y * invz)
    )

  let g = of_coordinates (Some g)
  let q = q

  let double (x1, y1, z1) =
    (* https://hyperelliptic.org/EFD/g1p/auto-shortw-projective.html#doubling-dbl-2007-bl *)
    let open F in
    let xx = square x1 in
    let zz = square z1 in
    let w = a * zz + double xx + xx in
    let s = double (y1 * z1) in
    let ss = square s in
    let sss = s * ss in
    let r = y1 * s in
    let rr = square r in
    let b = square (x1 + r) - xx - rr in
    let h = square w - double b in
    let x3 = h * s in
    let y3 = w * (b - h) - double rr in
    let z3 = sss in
    (x3, y3, z3)

  let add (x1, y1, z1) (x2, y2, z2) =
    (* https://hyperelliptic.org/EFD/g1p/auto-shortw-projective.html#addition-add-1998-cmo-2 *)
    let open F in
    let y1z2 = y1 * z2 in
    let x1z2 = x1 * z2 in
    let z1z2 = z1 * z2 in
    let u = y2 * z1 - y1z2 in
    let uu = u * u in
    let v = x2 * z1 - x1z2 in
    let vv = v * v in
    let vvv = v * vv in
    let r = vv * x1z2 in
    let a = uu * z1z2 - vvv - double r in
    let x3 = v * a in
    let y3 = u * (r - a) - vvv * y1z2 in
    let z3 = vvv * z1z2 in
    (x3, y3, z3)

  let ( *~ ) ((x1, y1, z1) as p1) ((x2, y2, z2) as p2) =
    if is_one p1 then
      p2
    else if is_one p2 then
      p1
    else if F.(compare (x1 * z2) (x2 * z1)) <> 0 then
      add p1 p2
    else if F.(compare (y1 * z2) (y2 * z1)) = 0 then
      double p1
    else
      one

  let windowsize = 4
  let windowmask = (1 lsl windowsize) - 1
  let windowmaskZ = Z.of_int windowmask
  let windowiterations = int_of_float (ceil (255. /. float_of_int windowsize))

  let ( **~ ) p n =
    let t = Array.make (windowmask + 1) one in
    t.(1) <- p;
    let rec init i =
      if i < windowmask then (
        let z = t.(i / 2) in
        let s = double z in
        t.(i) <- s;
        t.(i + 1) <- s *~ p;
        init (i + 2)
      ) else ()
    in
    init 2;
    let rec loop i s =
      if i >= 0 then (
        let k = i * windowsize in
        let j = Z.(logand (shift_right n k) windowmaskZ |> to_int) in
        let s = s *~ t.(j) in
        let s =
          if i <> 0 then (
            let rec loop i s =
              if i > 0 then loop (i - 1) (double s) else s
            in
            loop windowsize s
          ) else s
        in
        loop (i - 1) s
      ) else s
    in
    loop (windowiterations - 1) one

  let compare (x1, y1, z1) (x2, y2, z2) =
    let a = F.(compare (x1 * z2) (x2 * z1)) in
    if a = 0 then F.(compare (y1 * z2) (y2 * z1)) else a

  let ( =~ ) p1 p2 = compare p1 p2 = 0

  let () = assert (g **~ q =~ one)

  let invert (x, y, z) =
    F.(x, zero - y, z)

  let to_string p =
    match to_coordinates p with
    | Some (x, y) -> Printf.sprintf "%s%%%s" (to_string_base16 x) (to_string_base16 y)
    | None -> "Infinity%Infinity"

  let of_string p =
    match String.split_on_char '%' p with
    | ["Infinity"; "Infinity"] -> one
    | [x; y] -> of_coordinates @@ Some (of_string_base16 x, of_string_base16 y)
    | _ -> invalid_arg "Short_weierstrass.of_string"

  let hash _ _ = assert false
  let hash_to_int _ = assert false

  let description = description
end
