(**************************************************************************)
(*                                  VVFE                                  *)
(*                                                                        *)
(*  Copyright © 2021-2022 Inria                                           *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU Affero General Public License as        *)
(*  published by the Free Software Foundation, either version 3 of the    *)
(*  License, or (at your option) any later version, with the additional   *)
(*  exemption that compiling, linking, and/or using OpenSSL is allowed.   *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful, but   *)
(*  WITHOUT ANY WARRANTY; without even the implied warranty of            *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *)
(*  Affero General Public License for more details.                       *)
(*                                                                        *)
(*  You should have received a copy of the GNU Affero General Public      *)
(*  License along with this program.  If not, see                         *)
(*  <http://www.gnu.org/licenses/>.                                       *)
(**************************************************************************)

open Js_of_ocaml
open Js_of_ocaml_tyxml
open Tyxml_js.Html5
open Vvfe_web.Common
open Vvfe
open Common

let check_cachet body ~pubkey ~operation ~tour cachet =
  match Individual.check_cachet ~pubkey ~operation ~tour cachet with
  | Ok (election, etablissement, reference, check) ->
     if check () then (
       set_className_on_textareas "cachet_valide";
       let* () = log ~cachet "cachet-valide" in
       let@ () = show_in body in
       Lwt.return [
           div [
               div ~a:[a_id "div_cachet_valide"] [
                   span ~a:[a_class ["cachet_valide"]] [
                       txt "Ce cachet est valide pour la ";
                       txt (get_election_name election);
                       txt " (";
                       txt (get_etablissement_name etablissement);
                       txt ") du tour ";
                       txt (string_of_int tour);
                       txt ".";
                     ];
                 ];
               ul [
                   li [
                       txt "La référence du bulletin associé est ";
                       format_reference reference;
                       txt ". ";
                       txt "Elle doit être identique aux deux références affichées à l'écran ";
                       txt "juste après le vote."
                     ];
                   li [
                       txt "Revenez après le dépouillement pour vérifier ";
                       txt "que ce bulletin a bien été compté.";
                     ];
                 ];
             ]
         ]
     ) else (
       set_className_on_textareas "cachet_invalide";
       let* () = log "cachet-invalide" in
       let@ () = show_in body in
       Lwt.return [
           div [
               div ~a:[a_class ["span_bold"; "cachet_invalide"]] [
                   txt "Ce cachet est invalide pour le tour ";
                   txt (string_of_int tour);
                   txt ".";
                 ];
               txt " ";
               txt "Nous ne serons pas en mesure d'effectuer la vérification. ";
               txt "Vous pouvez vous ";
               a ~a:[a_href "../../aide-meae.html"] [
                   txt "rapprocher des autorités de l'élection";
                 ];
               txt ".";
             ]
         ]
     )
  | Error msg -> syntax_error body tour msg

let onload () =
  let pubkey = Js.to_string @@ Js.Unsafe.pure_js_expr "vvfe_cachet_pk" in
  let operation : int = Js.Unsafe.pure_js_expr "vvfe_operation" in
  let tour : int = Js.Unsafe.pure_js_expr "vvfe_tour" in
  let title = "Vérifiabilité individuelle" in
  let tour_as_str =
    match tour with
    | 1 -> "Premier tour"
    | 2 -> "Second tour"
    | n -> Printf.sprintf "Tour n°%d" n
  in
  let subtitle = "Élections législatives partielles 2023 — " ^ tour_as_str in
  document##.title := Js.string title;
  let subsubtitle =
    if tour = 2 then
      div ~a:[a_class ["subsubtitle"]] [
          txt "(Le site pour le ";
          a ~a:[a_href "../../tour1/verif/"] [txt "premier tour"];
          txt " est encore disponible.)";
      ]
    else
      txt ""
  in

  let body = div [] in
  let body_dom = Tyxml_js.To_dom.of_div body in
  let* () =
    let@ () = show_in body_dom in
    cachet_input_form (check_cachet body_dom ~pubkey ~operation ~tour)
  in
  let* () =
    let@ () = show_in document##.body in
    Lwt.return [
        div ~a:[a_class ["titre"]] [
            div ~a:[a_class ["titlebox"]] [
                txt title;
                br();
                txt subtitle;
                subsubtitle;
            ];
          ];
        div ~a:[a_class ["page"]] [
            div [
                txt "Pendant le scrutin, vous pouvez vérifier la validité ";
                txt "du cachet de votre bulletin. ";
                txt "Nous vérifierons après le dépouillement que votre ";
                txt "bulletin a bien été compté. ";
                txt "Vous pourrez également venir vérifier vous-même ";
                a ~a:[a_href "../../verif.html"] [txt "sur ce site"];
                txt ".";
                br();
                txt "Le cachet apparait sur le récépissé de votre bulletin.";
              ];
            div [
                a ~a:[a_href "../../informations.html"] [txt "Plus d'information"];
              ];
            body;
            footer;
          ]
      ]
  in
  Lwt.return_unit

let () = Dom_html.window##.onload := lwt_handler onload
