(**************************************************************************)
(*                                  VVFE                                  *)
(*                                                                        *)
(*  Copyright © 2021-2022 Inria                                           *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU Affero General Public License as        *)
(*  published by the Free Software Foundation, either version 3 of the    *)
(*  License, or (at your option) any later version, with the additional   *)
(*  exemption that compiling, linking, and/or using OpenSSL is allowed.   *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful, but   *)
(*  WITHOUT ANY WARRANTY; without even the implied warranty of            *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *)
(*  Affero General Public License for more details.                       *)
(*                                                                        *)
(*  You should have received a copy of the GNU Affero General Public      *)
(*  License along with this program.  If not, see                         *)
(*  <http://www.gnu.org/licenses/>.                                       *)
(**************************************************************************)

open Js_of_ocaml
open Js_of_ocaml_lwt
open Js_of_ocaml_tyxml
open Tyxml_js.Html5
open Vvfe
open Common

let document = Dom_html.document

let ( let* ) = Lwt.bind
let ( let@ ) x f = x f

let ( let& ) x f =
  match x with
  | None -> Lwt.return_unit
  | Some x -> f x

let lwt_handler f =
  Dom_html.handler (fun _ -> Lwt.async f; Js._true)

let show_in container f =
  let* content = f () in
  let content = List.map Tyxml_js.To_dom.of_node content in
  List.iter (Dom.appendChild container) content;
  Lwt.return_unit

let textarea ~cols ~rows ~placeholder=
  let elt = textarea ~a:[a_cols cols; a_rows rows; a_placeholder placeholder] (txt "") in
  let dom = Tyxml_js.To_dom.of_textarea elt in
  let get () = dom##.value |> Js.to_string in
  let disable () = dom##.disabled := Js._true in
  elt, get, disable

let button label handler =
  let elt = button [txt label] in
  let dom = Tyxml_js.To_dom.of_button elt in
  Lwt.async (fun () ->
      let* _ = Lwt_js_events.click dom in
      handler ()
    );
  elt

let log ?msg ?cachet state =
  let msg =
    match msg with
    | None -> []
    | Some msg -> ["msg", msg]
  in
  let cachet =
    match cachet with
    | None -> []
    | Some cachet -> ["cachet", cachet]
  in
  let params = Url.encode_arguments (msg @ cachet) in
  let url = Printf.sprintf "logs/%s?%s" state params in
  let* _ = XmlHttpRequest.get url in
  Lwt.return_unit

let set_className_on_textareas className =
  let textareas = document##querySelectorAll (Js.string "textarea") in
  for i = 0 to textareas##.length - 1 do
    Js.Opt.iter
      (textareas##item i)
      (fun x -> x##.className := Js.string className)
  done

let syntax_error body tour msg =
  set_className_on_textareas "cachet_invalide";
  let* () = log ~msg "syntax-error" in
  let@ () = show_in body in
  let b =
    let@ () = button "Recommencer" in
    Dom_html.window##.location##reload;
    Lwt.return_unit
  in
  Lwt.return [
      div ~a:[a_id "div_cachet_invalide"] [
          div [
              div ~a:[a_class ["span_bold"; "cachet_invalide"]] [
                  txt "Le format du cachet est invalide pour le tour ";
                  txt (string_of_int tour);
                  txt ".";
                ];
              txt "Vous avez probablement fait une erreur de copier-coller (voir ci-dessous). ";
              txt "Si le problème persiste, veuillez ";
              a ~a:[a_href "../../aide-meae.html"] [
                  txt "contacter l'assistance";
                ];
              txt " et fournir votre récépissé de vote.";
            ];
          b;
        ];
      div ~a:[a_id "imgaide"] [
          img ~a:[a_width 512; a_height 642] ~src:"aide.png" ~alt:"Aide" ();
        ];
    ]

let kill id =
  let& x = Dom_html.getElementById_opt id in
  let& parent = x##.parentNode |> Js.Opt.to_option in
  Dom.removeChild parent x;
  Lwt.return_unit

let cachet_input_form handler =
  let t, tget, tdisable = textarea ~cols:95 ~rows:9 ~placeholder:"Copiez-collez votre cachet ici" in
  let b =
    let@ () = button "Vérifier" in
    let* () = kill "div_button_validate" in
    tdisable ();
    handler (tget ())
  in
  Lwt.return [
      div ~a:[a_class ["div_cachet_prompt"]] [txt "Veuillez entrer le cachet de votre bulletin :"];
      div ~a:[a_class ["div_cachet_input"]] [
          div ~a:[a_class ["cachet"]]
            [
              img ~a:[a_width 51; a_height 70] ~src:"seal.png" ~alt:"Cachet" ();
              t
            ];
          div ~a:[a_id "div_button_validate"] [b];
        ];
    ]

let get_election_name i =
  let expr = Printf.sprintf {|vvfe_election_map["elections"]["%d"]|} i in
  Js.Unsafe.pure_js_expr expr |> Js.to_string

let get_etablissement_name i =
  let expr = Printf.sprintf {|vvfe_election_map["etablissements"]["%d"]|} i in
  Js.Unsafe.pure_js_expr expr |> Js.to_string

let footer =
  div ~a:[a_class ["footer"]] [
      a ~a:[a_href "../../legal.html"] [txt "Mentions légales"];
      txt " ";
      a ~a:[a_href "../../aide-meae.html"] [txt "Assistance MEAE"];
    ]

let format_reference reference =
  let content =
    match String.split_on_char '&' reference with
    | [id; election; hash] ->
       [
         span ~a:[a_class ["ref_minor"]] [txt @@ Printf.sprintf "%s&%s&" id election];
         span ~a:[a_class ["ref_major"]] [txt hash];
       ]
    | _ -> [txt reference]
  in
  span ~a:[a_class ["span_ref"]] content
