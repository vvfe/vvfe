(**************************************************************************)
(*                                  VVFE                                  *)
(*                                                                        *)
(*  Copyright © 2021-2022 Inria                                           *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU Affero General Public License as        *)
(*  published by the Free Software Foundation, either version 3 of the    *)
(*  License, or (at your option) any later version, with the additional   *)
(*  exemption that compiling, linking, and/or using OpenSSL is allowed.   *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful, but   *)
(*  WITHOUT ANY WARRANTY; without even the implied warranty of            *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *)
(*  Affero General Public License for more details.                       *)
(*                                                                        *)
(*  You should have received a copy of the GNU Affero General Public      *)
(*  License along with this program.  If not, see                         *)
(*  <http://www.gnu.org/licenses/>.                                       *)
(**************************************************************************)

open Js_of_ocaml
open Js_of_ocaml_lwt
open Js_of_ocaml_tyxml
open Tyxml_js.Html5
open Vvfe_web.Common
open Vvfe
open Common

module Loader = struct
  let load path =
    let filename =
      match path with
      | `Root -> "fingerprints-root.json"
      | `Index i -> Printf.sprintf "%d/fingerprints-index.json" i
      | `File (i, x) -> Printf.sprintf "%d/fingerprints-%s.txt" i x
    in
    let* x = XmlHttpRequest.get filename in
    Firebug.console##log (Js.string x.content);
    Lwt.return x.content
end

module LwtYield = struct
  include Lwt
  let yield = Lwt_js.yield
end

module X = Individual.Make (LwtYield) (Loader)

let check_cachet body ~rootfp ~pubkey ~operation ~tour cachet =
  match Individual.check_cachet ~pubkey ~operation ~tour cachet with
  | Ok (election, etablissement, reference, check) ->
     begin
       let* r = X.check_reference ~rootfp ~operation ~tour reference in
       match r with
       | Ok (_encrypted, invalid) ->
          let@ () = show_in body in
          let invalid, className =
            if invalid then
              "enregistré, mais pas pris en compte car il est nul,", "cachet_invalide"
            else
              "pris en compte", "cachet_valide"
          in
          let* () = log ~msg:className "cachet-valide" in
          set_className_on_textareas className;
          let fingerprints =
            Printf.sprintf "../rapport/fingerprints/fingerprints-%d.txt" election
          in
          Lwt.return [
              div [
                  div ~a:[a_id "div_cachet_valide"] [
                      span ~a:[a_class [className]] [
                          txt "Le bulletin correspondant à ce cachet a bien été ";
                          txt invalid;
                          txt " dans la ";
                          txt (get_election_name election);
                          txt " (";
                          txt (get_etablissement_name etablissement);
                          txt ")."
                        ];
                    ];
                  ul [
                      li [
                          txt "Le rapport de vérifiabilité avec les résultats est disponible ";
                          a ~a:[a_href "../rapport/rapport.pdf"; a_target "rapport"] [txt "ici"];
                          txt ".";
                        ];
                      li [
                          txt "La référence de ce bulletin est ";
                          format_reference reference;
                          txt ". ";
                          txt "Elle doit être identique aux deux références affichées à l'écran ";
                          txt "juste après le vote."
                        ];
                      li [
                          txt "La liste complète des références des bulletins acceptés est disponible ";
                          a ~a:[a_href fingerprints; a_target "fingerprints"] [txt "ici"];
                          txt ".";
                        ];
                    ];
                ]
            ]
       | Error (`Syntax msg) -> syntax_error body tour msg
       | Error (`Server msg) ->
          let* () = log ~msg "server-error" in
          let@ () = show_in body in
          Lwt.return [
              div [
                  txt "Erreur du serveur. ";
                  txt "Veuillez réessayer ultérieurement.";
                ]
            ]
       | Error `Missing ->
          set_className_on_textareas "cachet_invalide";
          if check () then (
            let* () = log ~cachet "missing-but-valid" in
            let@ () = show_in body in
            Lwt.return [
                div [
                    div ~a:[a_class ["span_bold"; "cachet_invalide"]] [
                        txt "Attention, ce bulletin n'a pas été compté."
                      ];
                    txt "Le cachet est cependant valide, ";
                    txt "nous vous recommandons de ";
                    a ~a:[a_href "../../aide-meae.html"] [
                        txt "contacter les autorités de l'élection";
                      ];
                    txt " en leur fournissant votre récépissé.";
                  ]
              ]
          ) else (
            let* () = log "missing-and-invalid" in
            let@ () = show_in body in
            Lwt.return [
                div [
                    div ~a:[a_class ["span_bold"; "cachet_invalide"]] [
                        txt "Le cachet est invalide."
                      ];
                    txt "Nous ne sommes pas en mesure d'effectuer la vérification. ";
                    txt "Si le problème persiste, veuillez ";
                    a ~a:[a_href "../../aide-meae.html"] [
                        txt "contacter l'assistance";
                      ];
                    txt " et fournir votre récépissé de vote.";
                  ]
              ]
          )
     end
  | Error msg -> syntax_error body tour msg

let onload () =
  let rootfp = Js.to_string @@ Js.Unsafe.pure_js_expr "vvfe_rootfp" in
  let pubkey = Js.to_string @@ Js.Unsafe.pure_js_expr "vvfe_cachet_pk" in
  let operation : int = Js.Unsafe.pure_js_expr "vvfe_operation" in
  let tour : int = Js.Unsafe.pure_js_expr "vvfe_tour" in
  let title = "Vérifiabilité individuelle" in
  let tour_as_str =
    match tour with
    | 1 -> "Premier tour"
    | 2 -> "Second tour"
    | n -> Printf.sprintf "Tour n°%d" n
  in
  let subtitle = "Élections législatives partielles 2023 — " ^ tour_as_str in
  document##.title := Js.string title;
  let subsubtitle =
    if tour = 2 then
      div ~a:[a_class ["subsubtitle"]] [
          txt "(Le site pour le ";
          a ~a:[a_href "../../tour1/verif/"] [txt "premier tour"];
          txt " est encore disponible.)";
      ]
    else
      txt ""
  in
  let body = div [] in
  let body_dom = Tyxml_js.To_dom.of_div body in
  let* () =
    let@ () = show_in body_dom in
    cachet_input_form (check_cachet body_dom ~rootfp ~pubkey ~operation ~tour)
  in
  let* () =
    let@ () = show_in document##.body in
    Lwt.return [
        div ~a:[a_class ["titre"]] [
            div ~a:[a_class ["titlebox"]] [
                txt title;
                br();
                txt subtitle;
                subsubtitle;
            ];
        ];
        div ~a:[a_class ["page"]] [
            div [
                txt "En tant que tiers, nous avons eu accès à l'ensemble des ";
                txt "bulletins dépouillés et nous avons vérifié qu'ils ";
                txt "correspondent aux résultats de l'élection. Vous pouvez ";
                txt "vérifier ici que votre bulletin a bien été compté ";
                txt "dans votre circonscription.";
                br();
                txt "Le cachet apparait sur le récépissé de votre bulletin.";
              ];
            div [
                a ~a:[a_href "../../informations.html"] [txt "Plus d'information"];
            ];
            body;
            footer;
        ];
    ]
  in
  Lwt.return_unit

let () = Dom_html.window##.onload := lwt_handler onload
