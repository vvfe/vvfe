(**************************************************************************)
(*                                  VVFE                                  *)
(*                                                                        *)
(*  Copyright © 2012-2021 Inria                                           *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU Affero General Public License as        *)
(*  published by the Free Software Foundation, either version 3 of the    *)
(*  License, or (at your option) any later version, with the additional   *)
(*  exemption that compiling, linking, and/or using OpenSSL is allowed.   *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful, but   *)
(*  WITHOUT ANY WARRANTY; without even the implied warranty of            *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *)
(*  Affero General Public License for more details.                       *)
(*                                                                        *)
(*  You should have received a copy of the GNU Affero General Public      *)
(*  License along with this program.  If not, see                         *)
(*  <http://www.gnu.org/licenses/>.                                       *)
(**************************************************************************)

open Vvfe_platform.Platform
open Vvfe_platform.Common
open Serializable_builtin_t
open Serializable_core_j
open Serializable_j
open Signatures
open Common

module Parse (R : RAW_ELECTION) () = struct
  let j = params_of_string Yojson.Safe.read_json R.raw_election
  module G = (val Group.of_string j.e_group)
  let params = params_of_string G.read R.raw_election

  let election = params
  let fingerprint = sha256_b64 R.raw_election
  let public_key = params.e_trustees.trustees_overall_key

  type nonrec ballot = G.t ballot
  let string_of_ballot x = string_of_ballot G.write x
  let ballot_of_string x = ballot_of_string G.read x

end

module MakeElection (W : ELECTION_DATA) (M : RANDOM) = struct
  type 'a m = 'a M.t
  let ( let* ) = M.bind

  type elt = W.G.t

  module G = W.G
  module Q = Question.Make (M) (G) (Question_h.Make (M) (G))
  open G
  let election = W.election

  type private_key = Z.t
  type public_key = elt

  let ( / ) x y = x *~ invert y

  type plaintext = int array array
  type nonrec ballot = elt ballot

  (** Fiat-Shamir non-interactive zero-knowledge proofs of
      knowledge *)

  let fs_prove gs x oracle =
    let* w = M.random q in
    let commitments = Array.map (fun g -> g **~ w) gs in
    let* () = M.yield () in
    let challenge = oracle commitments in
    let response = Z.(erem (w + x * challenge) q) in
    M.return {challenge; response}

  (** Ballot creation *)

  let swap xs =
    let rec loop i accu =
      if i >= 0
      then let* x = xs.(i) in loop (pred i) (x::accu)
      else M.return (Array.of_list accu)
    in loop (pred (Array.length xs)) []

  let create_answer y zkp q m =
    Q.create_answer q ~public_key:y ~prefix:zkp m

  let generate_tokenId =
    let z = Z.(shift_left one 128) in
    fun () ->
    let* x = M.random z in
    let x = Z.to_string x |> sha256_hex in
    M.return (String.sub x 0 32)

  let create_ballot m =
    let electionUUID = election.e_uuid in
    let electionHash = W.fingerprint in
    let* tokenId = generate_tokenId () in
    let zkp = electionUUID ^ tokenId in
    let* answers = swap (Array.map2 (create_answer W.public_key zkp) election.e_questions m) in
    M.return
      {
        electionUUID;
        electionHash;
        answers;
        tokenId;
      }

  (** Ballot verification *)

  let verify_answer y zkp q a =
    Q.verify_answer q ~public_key:y ~prefix:zkp a

  let check_ballot {electionUUID; answers; tokenId; _} =
    let zkp = electionUUID ^ tokenId in
    (* Ideally, electionHash as specified in the ballot should be
       checked with something... but the specification is too vague
       and we don't know what to check. This does not seem
       cryptographically relevant, anyway. *)
    electionUUID = election.e_uuid
    && Array.forall2 (verify_answer W.public_key zkp) election.e_questions answers

  (** Tally *)

  type ciphertexts = elt ciphertext shape

  let extract_ciphertexts ballot =
    let open Shape in
    let open Question_h_t in
    SArray (
        Array.map (fun a ->
            SArray (
                Array.map (fun c ->
                    SAtomic c.cipherText
                  ) a.choices
              )
          ) ballot.answers
      )

  let neutral_ciphertext =
    {alpha = G.one; beta = G.one}

  let neutral_ciphertexts =
    let open Shape in
    let neutral = SAtomic neutral_ciphertext in
    SArray (
        Array.map (fun (Question.Homomorphic q) ->
            SArray (Array.make (Question_h.question_length q) neutral)
          ) election.e_questions
      )

  let combine a b =
    {
      alpha = a.alpha *~ b.alpha;
      beta = a.beta *~ b.beta;
    }

  let combine_ciphertexts = Shape.map2 combine

  type factor = elt partial_decryption

  let eg_factor x {alpha; _} =
    let zkp = "decrypt|" ^ G.to_string (g **~ x) ^ "|" in
    (alpha, alpha **~ x),
    fs_prove [| g; alpha |] x (hash zkp)

  let check_ciphertext c =
    Shape.forall (fun {alpha; beta} -> G.check alpha && G.check beta) c

  let rec swaps = function
    | SAtomic x -> let* x = x in M.return (SAtomic x)
    | SArray x ->
       let rec loop i accu =
         if i >= 0
         then let* x = swaps x.(i) in loop (pred i) (x::accu)
         else M.return (SArray (Array.of_list accu))
       in
       loop (pred (Array.length x)) []

  let compute_factor ~index c x =
    if check_ciphertext c then (
      let res = Shape.map (eg_factor x) c in
      let decryption_factors, decryption_proofs = Shape.split res in
      let* decryption_proofs = swaps decryption_proofs in
      Shape.combine decryption_factors decryption_proofs
      |> Shape.map (fun ((product_alpha, factor), proof) -> {product_alpha; factor; proof})
      |> (fun pd_items -> M.return {pd_items; pd_index = index})
    ) else (
      M.fail (Invalid_argument "Invalid ciphertext")
    )

  let check_factor c y f =
    let zkp = "decrypt|" ^ G.to_string y ^ "|" in
    Shape.forall2 (fun {alpha; _} {product_alpha; factor; proof = {challenge; response}} ->
        alpha =~ product_alpha
        && G.check factor
        && check_modulo q challenge
        && check_modulo q response
        && let commitments =
             [|
               g **~ response / y **~ challenge;
               alpha **~ response / factor **~ challenge;
             |]
           in Z.(hash zkp commitments =% challenge)
      ) c f.pd_items

  type result_type = W.result
  type result = (elt, result_type) Serializable_t.election_result

  module Combinator = Trustees.MakeCombinator (G)

  let compute_result encrypted_tally partial_decryptions =
    let num_tallied = encrypted_tally.et_length in
    let num_invalid = encrypted_tally.et_invalid in
    let trustees = election.e_trustees in
    let check = check_factor encrypted_tally.et_product in
    match Combinator.combine_factors trustees check partial_decryptions with
    | Ok factors ->
       let results =
         Shape.map2 (fun {beta; _} f ->
             beta / f
           ) encrypted_tally.et_product factors
       in
       let raw_result =
         match results with
         | SAtomic _ ->
            invalid_arg "Election.compute_result: cannot compute result"
         | SArray xs ->
            Array.map2 (Q.compute_result ~num_tallied) election.e_questions xs
       in
       let result = W.cast_result raw_result in
       Ok {num_tallied; num_invalid; encrypted_tally; partial_decryptions; result}
    | Error e -> Error e

  let check_result trustees r =
    let {num_tallied; num_invalid; encrypted_tally; partial_decryptions; result} = r in
    check_ciphertext encrypted_tally.et_product
    && num_tallied = encrypted_tally.et_length
    && num_invalid = encrypted_tally.et_invalid
    && let check = check_factor encrypted_tally.et_product in
       match Combinator.combine_factors trustees check partial_decryptions with
       | Error _ -> false
       | Ok factors ->
          let results =
            Shape.map2 (fun {beta; _} f ->
                beta / f
              ) encrypted_tally.et_product factors
          in
          match results with
          | SArray xs ->
             Array.forall3 (Q.check_result ~num_tallied) election.e_questions xs (result : W.result :> raw_result)
          | _ -> false
end

module Make (MakeResult : MAKE_RESULT) (R : RAW_ELECTION) (M : RANDOM) () = struct
  module X = Parse (R) ()
  module Y = struct
    include X
    include MakeResult (X)
  end
  include Y
  type 'a m = 'a M.t
  module E = MakeElection (Y) (M)
end
