(**************************************************************************)
(*                                  VVFE                                  *)
(*                                                                        *)
(*  Copyright © 2012-2021 Inria                                           *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU Affero General Public License as        *)
(*  published by the Free Software Foundation, either version 3 of the    *)
(*  License, or (at your option) any later version, with the additional   *)
(*  exemption that compiling, linking, and/or using OpenSSL is allowed.   *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful, but   *)
(*  WITHOUT ANY WARRANTY; without even the implied warranty of            *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *)
(*  Affero General Public License for more details.                       *)
(*                                                                        *)
(*  You should have received a copy of the GNU Affero General Public      *)
(*  License along with this program.  If not, see                         *)
(*  <http://www.gnu.org/licenses/>.                                       *)
(**************************************************************************)

(** Signatures *)

open Vvfe_platform
open Platform
open Serializable_t

(** Helpers for interacting with atd stuff *)

type 'a reader = Yojson.Safe.lexer_state -> Lexing.lexbuf -> 'a
type 'a writer = Buffer.t -> 'a -> unit

module type GROUP = Signatures_core.GROUP
module type MONAD = Signatures_core.MONAD
module type RANDOM = Signatures_core.RANDOM

module type ELECTION_BASE = sig
  module G : GROUP
  val election : G.t params
  val fingerprint : string
  val public_key : G.t

  type ballot
  val string_of_ballot : ballot -> string
  val ballot_of_string : string -> ballot
end

module type ELECTION_RESULT = sig
  type result = private raw_result
  val cast_result : raw_result -> result
  val write_result : result writer
  val read_result : result reader
end

module type MAKE_RESULT = functor (X : ELECTION_BASE) -> ELECTION_RESULT

module type ELECTION_DATA = sig
  include ELECTION_BASE
  include ELECTION_RESULT
end

type combination_error =
  | MissingPartialDecryption
  | NotEnoughPartialDecryptions
  | UnusedPartialDecryption

module type RAW_ELECTION = sig
  val raw_election : string
end

(** Cryptographic primitives for an election with homomorphic tally. *)
module type ELECTION_OPS = sig

  type 'a m
  (** The type of monadic values. *)

  (** {2 Election parameters} *)

  (** Ballots are encrypted using public-key cryptography secured by
      the discrete logarithm problem. Here, we suppose private keys
      are integers modulo a large prime number. Public keys are
      members of a suitably chosen group. *)

  type elt

  type private_key = Z.t
  type public_key = elt

  (** {2 Ballots} *)

  type plaintext = Serializable_t.plaintext
  (** The plaintext equivalent of [ciphertext], i.e. the contents of a
      ballot. When [x] is such a value, [x.(i).(j)] is the value (0
      or 1) given to answer [j] in question [i]. *)

  type ballot
  (** A ballot ready to be transmitted, containing the encrypted
      answers and cryptographic proofs that they satisfy the election
      constraints. *)

  val create_ballot : plaintext -> ballot m
  (** [create_ballot r answers] creates a ballot, or raises
      [Invalid_argument] if [answers] doesn't satisfy the election
      constraints. *)

  val check_ballot : ballot -> bool
  (** [check_ballot b] checks all the cryptographic proofs in [b]. All
      ballots produced by [create_ballot] should pass this check. *)

  (** {2 Tally} *)

  type ciphertexts = elt Serializable_t.ciphertext shape
  val extract_ciphertexts : ballot -> ciphertexts
  val neutral_ciphertexts : ciphertexts
  val combine_ciphertexts : ciphertexts -> ciphertexts -> ciphertexts

  (** {2 Partial decryptions} *)

  type factor = elt partial_decryption
  (** A decryption share. It is computed by a trustee from his or her
      private key share and the encrypted tally, and contains a
      cryptographic proof that he or she didn't cheat. *)

  val compute_factor : index:int -> elt Serializable_t.ciphertext shape -> private_key -> factor m

  val check_factor : elt Serializable_t.ciphertext shape -> public_key -> factor -> bool
  (** [check_factor c pk f] checks that [f], supposedly submitted by a
      trustee whose public_key is [pk], is valid with respect to the
      encrypted tally [c]. *)

  (** {2 Result} *)

  type result_type
  type result = (elt, result_type) Serializable_t.election_result
  (** The election result. It contains the needed data to validate the
      result from the encrypted tally. *)

  val compute_result :
    elt Serializable_t.encrypted_tally -> factor list ->
    (result, combination_error) Stdlib.result
  (** Combine the encrypted tally and the factors from all trustees to
      produce the election result. The first argument is the number of
      tallied ballots. May raise [Invalid_argument]. *)

  val check_result : elt trustees -> result -> bool
end

module type ELECTION = sig
  include ELECTION_DATA
  type 'a m
  module E : ELECTION_OPS with type elt = G.t and type 'a m = 'a m and type ballot = ballot and type result_type = result
end
