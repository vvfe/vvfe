(**************************************************************************)
(*                                  VVFE                                  *)
(*                                                                        *)
(*  Copyright © 2012-2021 Inria                                           *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU Affero General Public License as        *)
(*  published by the Free Software Foundation, either version 3 of the    *)
(*  License, or (at your option) any later version, with the additional   *)
(*  exemption that compiling, linking, and/or using OpenSSL is allowed.   *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful, but   *)
(*  WITHOUT ANY WARRANTY; without even the implied warranty of            *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *)
(*  Affero General Public License for more details.                       *)
(*                                                                        *)
(*  You should have received a copy of the GNU Affero General Public      *)
(*  License along with this program.  If not, see                         *)
(*  <http://www.gnu.org/licenses/>.                                       *)
(**************************************************************************)

open Serializable_builtin_t
open Serializable_j
open Signatures
open Common

module MakeResult (X : ELECTION_BASE) = struct
  open X
  type result = raw_result

  let cast_result x =
    let questions = election.e_questions in
    let n = Array.length questions in
    if Array.length x = n then (
      let rec check i =
        if i < n then (
          match questions.(i), x.(i) with
          | Homomorphic _, RHomomorphic _ -> check (i + 1)
        ) else ()
      in
      check 0;
      x
    ) else failwith "cast_result: length mismatch"

  let write_result = write_raw_result

  let read_result state buf =
    let int_of_json = function
      | `Int i -> i
      | _ -> invalid_arg "read_result: int expected"
    in
    match Yojson.Safe.from_lexbuf ~stream:true state buf with
    | `List xs ->
       let n = Array.length election.e_questions in
       let result = Array.make n (RHomomorphic [||]) in
       let rec loop i xs =
         match (i < n), xs with
         | true, (x :: xs) ->
            (match election.e_questions.(i) with
             | Homomorphic _ ->
                (match x with
                 | `List ys ->
                    ys
                    |> Array.of_list
                    |> Array.map int_of_json
                    |> (fun x -> result.(i) <- RHomomorphic x)
                    |> (fun () -> loop (i + 1) xs)
                 | _ -> failwith "read_result/Homomorphic: list expected"
                )
            )
         | true, [] -> failwith "read_result: list too short"
         | false, _ :: _ -> failwith "read_result: list too long"
         | false, [] -> ()
       in
       loop 0 xs;
       result
    | _ -> failwith "read_result: list expected"

end

(** Helper functions *)

module Make (R : RAW_ELECTION) (M : RANDOM) () = struct
  include Election_crypto.Make (MakeResult) (R) (M) ()
end
