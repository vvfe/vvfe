(**************************************************************************)
(*                                  VVFE                                  *)
(*                                                                        *)
(*  Copyright © 2012-2021 Inria                                           *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU Affero General Public License as        *)
(*  published by the Free Software Foundation, either version 3 of the    *)
(*  License, or (at your option) any later version, with the additional   *)
(*  exemption that compiling, linking, and/or using OpenSSL is allowed.   *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful, but   *)
(*  WITHOUT ANY WARRANTY; without even the implied warranty of            *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *)
(*  Affero General Public License for more details.                       *)
(*                                                                        *)
(*  You should have received a copy of the GNU Affero General Public      *)
(*  License along with this program.  If not, see                         *)
(*  <http://www.gnu.org/licenses/>.                                       *)
(**************************************************************************)

open Signatures_core

type question =
  | Homomorphic of Question_h_t.question

let read_question l b =
  let x = Question_h_j.read_question l b in
  Homomorphic x

let write_question b = function
  | Homomorphic q -> Question_h_j.write_question b q

module Make (M : RANDOM) (G : GROUP)
         (QHomomorphic : Question_sigs.QUESTION
          with type 'a m := 'a M.t
           and type elt := G.t
           and type question := Question_h_t.question
           and type answer := G.t Question_h_t.answer) = struct

  let create_answer q ~public_key ~prefix m =
    match q with
    | Homomorphic q ->
       QHomomorphic.create_answer q ~public_key ~prefix m

  let verify_answer q ~public_key ~prefix a =
    match q with
    | Homomorphic q ->
       a
       |> QHomomorphic.verify_answer q ~public_key ~prefix

  let compute_result ~num_tallied =
    let compute_h = lazy (QHomomorphic.compute_result ~num_tallied) in
    fun q x ->
    match q with
    | Homomorphic q -> Lazy.force compute_h q x

  let check_result ~num_tallied q x r =
    match q with
    | Homomorphic q -> QHomomorphic.check_result ~num_tallied q x r
end
