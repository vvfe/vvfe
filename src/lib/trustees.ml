(**************************************************************************)
(*                                  VVFE                                  *)
(*                                                                        *)
(*  Copyright © 2012-2021 Inria                                           *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU Affero General Public License as        *)
(*  published by the Free Software Foundation, either version 3 of the    *)
(*  License, or (at your option) any later version, with the additional   *)
(*  exemption that compiling, linking, and/or using OpenSSL is allowed.   *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful, but   *)
(*  WITHOUT ANY WARRANTY; without even the implied warranty of            *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *)
(*  Affero General Public License for more details.                       *)
(*                                                                        *)
(*  You should have received a copy of the GNU Affero General Public      *)
(*  License along with this program.  If not, see                         *)
(*  <http://www.gnu.org/licenses/>.                                       *)
(**************************************************************************)

open Vvfe_platform.Platform
open Serializable_j
open Signatures
open Common

let string_of_combination_error = function
  | MissingPartialDecryption -> "a partial decryption is missing"
  | NotEnoughPartialDecryptions -> "not enough partial decryptions"
  | UnusedPartialDecryption -> "unused partial decryption"

exception CombinationError of combination_error

module MakeCombinator (G : GROUP) = struct

  let rec firstn n accu xs =
    if n > 0 then (
      match xs with
      | [] -> failwith "inconsistency in Trustees.MakeCombinator"
      | x :: xs -> firstn (n - 1) (x :: accu) xs
    ) else (accu, xs)

  let lagrange indexes j x =
    List.fold_left (fun accu k ->
        let kx = k - x in
        let kj = k - j in
        if kj = 0 then accu
        else Z.(erem (accu * (of_int kx) * invert (of_int kj) G.q) G.q)
      ) Z.one indexes

  let combine pks_with_indexes x =
    let indexes = List.map fst pks_with_indexes in
    List.fold_left
      (fun a (j, b) ->
        let l = lagrange indexes j x in
        G.(a *~ b **~ l)
      ) G.one pks_with_indexes

  let check a =
    let pk = a.trustees_overall_key in
    let pks = a.trustees_individual_keys in
    let n = List.length pks in
    G.check pk
    && List.for_all G.check pks
    && match a.trustees_threshold with
       | None -> true
       | Some t ->
          0 < t && t <= n
          && let xs, ys =
               List.mapi (fun i pk -> i + 1, pk) pks
               |> firstn t []
             in
             G.(pk =~ combine xs 0)
             && List.for_all (fun (i, pk) -> G.(pk =~ combine xs i)) ys

  let combine_factors_exc trustees check partial_decryptions =
    let threshold =
      match trustees.trustees_threshold with
      | None -> failwith "missing threshold in combine_factors_exc"
      | Some t -> t
    in
    (* neutral factor *)
    let dummy =
      match partial_decryptions with
      | x :: _ -> Shape.map (fun _ -> G.one) x.pd_items
      | [] -> failwith "no partial decryptions"
    in
    let partial_decryptions =
      List.map (fun x -> x, ref true) partial_decryptions
    in
    let check i t (x, unchecked) =
      if !unchecked then (
        let r = x.pd_index = i + 1 && check t x in
        if r then unchecked := false;
        r
      ) else false
    in
    let factors =
      let rec take n accu xs =
        if n > 0 then
          match xs with
          | [] -> raise (CombinationError NotEnoughPartialDecryptions)
          | x :: xs -> take (n-1) (x :: accu) xs
        else accu
      in
      let pds_with_ids =
        trustees.trustees_individual_keys
        |> List.mapi (fun i t -> List.find_opt (check i t) partial_decryptions)
        |> List.mapi
             (fun i x ->
               match x with
               | Some (x, _) ->
                  let index = i + 1 in
                  [index, x.pd_items]
               | None -> []
             )
        |> List.flatten
        |> take threshold []
      in
      let indexes = List.map fst pds_with_ids in
      List.fold_left
        (fun a (j, b) ->
          let l = lagrange indexes j 0 in
          Shape.map2 G.(fun x y -> x *~ y.factor **~ l) a b
        ) dummy pds_with_ids
    in
    (* check that all partial decryptions have been used *)
    if List.exists (fun (_, x) -> !x) partial_decryptions then
      raise (CombinationError UnusedPartialDecryption);
    factors

  let combine_factors trustees check partial_decryptions =
    try Ok (combine_factors_exc trustees check partial_decryptions)
    with CombinationError e -> Error e

end

(** Distributed key generation *)

module MakeKGShamir (G : GROUP) (M : RANDOM) = struct
  open G

  let ( let* ) = M.bind

  let generate ~nb_trustees ~threshold =
    let* polynomial =
      let rec loop n accu =
        if n > 0 then (
          let* a = M.random q in
          loop (n - 1) (a :: accu)
        ) else M.return accu
      in
      loop threshold []
    in
    let eval i =
      let base = Z.of_int i in
      let rec loop accu = function
        | [] -> accu
        | x :: xs -> loop Z.((accu * base + x) mod G.q) xs
      in
      loop Z.zero polynomial
    in
    let trustees_overall_key = g **~ eval 0 in
    let private_keys =
      let rec loop i accu =
        if i > 0 then
          loop (i - 1) (eval i :: accu)
        else
          accu
      in
      loop nb_trustees []
    in
    let trustees_individual_keys =
      List.map (fun x -> g **~ x) private_keys
    in
    M.return (
        {
          trustees_overall_key;
          trustees_individual_keys;
          trustees_threshold = Some threshold;
        },
        private_keys
      )

end
