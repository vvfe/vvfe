(**************************************************************************)
(*                                  VVFE                                  *)
(*                                                                        *)
(*  Copyright © 2012-2021 Inria                                           *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU Affero General Public License as        *)
(*  published by the Free Software Foundation, either version 3 of the    *)
(*  License, or (at your option) any later version, with the additional   *)
(*  exemption that compiling, linking, and/or using OpenSSL is allowed.   *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful, but   *)
(*  WITHOUT ANY WARRANTY; without even the implied warranty of            *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *)
(*  Affero General Public License for more details.                       *)
(*                                                                        *)
(*  You should have received a copy of the GNU Affero General Public      *)
(*  License along with this program.  If not, see                         *)
(*  <http://www.gnu.org/licenses/>.                                       *)
(**************************************************************************)

open Vvfe_platform
open Platform
open Signatures_core

let ( let@ ) f x = f x

module Array = struct
  include Array

  let forall f a =
    let n = Array.length a in
    (let rec check i =
       if i >= 0 then f a.(i) && check (pred i)
       else true
     in check (pred n))

  let forall2 f a b =
    let n = Array.length a in
    n = Array.length b &&
      (let rec check i =
         if i >= 0 then f a.(i) b.(i) && check (pred i)
         else true
       in check (pred n))

  let forall3 f a b c =
    let n = Array.length a in
    n = Array.length b &&
      n = Array.length c &&
        (let rec check i =
           if i >= 0 then f a.(i) b.(i) c.(i) && check (pred i)
           else true
         in check (pred n))

  let map2 f a b =
    Array.mapi (fun i ai -> f ai b.(i)) a

  let map3 f a b c =
    Array.mapi (fun i ai -> f ai b.(i) c.(i)) a

  let combine a b =
    map2 (fun a b -> a, b) a b

end

module String = struct
  include String

  let startswith x s =
    let xn = String.length x and sn = String.length s in
    xn >= sn && String.sub x 0 sn = s
end

module List = struct
  include List

  let rec filter_map f = function
    | [] -> []
    | x :: xs ->
       let ys = filter_map f xs in
       match f x with
       | None -> ys
       | Some y -> y :: ys
end

module Shape = struct
  type 'a t =
    | SAtomic of 'a
    | SArray of 'a t array

  let to_array = function
    | SAtomic _ -> invalid_arg "Shape.to_array"
    | SArray xs ->
       Array.map (function
           | SAtomic x -> x
           | SArray _ -> invalid_arg "Shape.to_array"
         ) xs

  let rec map f = function
    | SAtomic x -> SAtomic (f x)
    | SArray x -> SArray (Array.map (map f) x)

  let rec map2 f a b =
    match a, b with
    | SAtomic x, SAtomic y -> SAtomic (f x y)
    | SArray x, SArray y -> SArray (Array.map2 (map2 f) x y)
    | _, _ -> invalid_arg "Shape.map2"

  let split x =
    map fst x, map snd x

  let rec combine x y =
    match x, y with
    | SAtomic x, SAtomic y -> SAtomic (x, y)
    | SArray xs, SArray ys ->
       Array.combine xs ys
       |> Array.map (fun (x, y) -> combine x y)
       |> (fun z -> SArray z)
    | _, _ -> invalid_arg "Shape.combine"

  let rec forall p = function
    | SAtomic x -> p x
    | SArray x -> Array.forall (forall p) x

  let rec forall2 p x y =
    match x, y with
    | SAtomic x, SAtomic y -> p x y
    | SArray x, SArray y -> Array.forall2 (forall2 p) x y
    | _, _ -> invalid_arg "Shape.forall2"

end

module SSet = Set.Make(String)
module SMap = Map.Make(String)

module ISet = Set.Make(Int)
module IMap = Map.Make(Int)

let sqrt s =
  (* https://en.wikipedia.org/wiki/Integer_square_root *)
  let rec loop x0 =
    let x1 = Z.(shift_right (x0 + s / x0) 1) in
    if Z.compare x1 x0 < 0 then loop x1 else x0
  in
  let x0 = Z.shift_right s 1 in
  if Z.compare x0 Z.zero > 0 then loop x0 else s

module BabyStepGiantStep (G : GROUP) = struct
  (* https://en.wikipedia.org/wiki/Baby-step_giant-step *)
  let log ~generator:alpha ~max:n =
    let m = Z.(to_int (sqrt n + one)) in
    let table = Hashtbl.create m in
    let add_to_table x i =
      let h = G.hash_to_int x in
      let ii =
        match Hashtbl.find_opt table h with
        | None -> []
        | Some ii -> ii
      in
      Hashtbl.add table h (i :: ii)
    in
    let rec populate_table j cur =
      if j < m then (
        add_to_table cur j;
        populate_table (j + 1) G.(cur *~ alpha)
      ) else cur
    in
    let inv_alpha_m = G.(invert (populate_table 0 one)) in
    fun beta ->
    let rec lookup i gamma =
      if i < m then (
        let r =
          match Hashtbl.find_opt table (G.hash_to_int gamma) with
          | Some jj ->
             let rec find = function
               | [] -> None
               | j :: jj ->
                  let r = Z.((of_int i * of_int m + of_int j) mod G.q) in
                  if G.(alpha **~ r =~ beta) then
                    Some r
                  else find jj
             in
             find jj
          | None -> None
        in
        match r with
        | Some r -> Some r
        | None -> lookup (i + 1) G.(gamma *~ inv_alpha_m)
      ) else None
    in
    lookup 0 beta
end

let split_lines str =
  let n = String.length str in
  let find i c =
    match String.index_from_opt str i c with
    | None -> n
    | Some j -> j
  in
  let rec loop accu i =
    if i < n then (
      let j = min (find i '\n') (find i '\r') in
      let line = String.sub str i (j-i) in
      let accu = if line = "" then accu else line :: accu in
      loop accu (j + 1)
    ) else List.rev accu
  in loop [] 0

let replace_underscores_by_spaces x =
  String.map (function '_' -> ' ' | c -> c) x

let files_of_dir dir =
  let ic = Unix.opendir dir in
  Fun.protect
    ~finally:(fun () -> Unix.closedir ic)
    begin
      fun () ->
      let rec loop accu =
        match Unix.readdir ic with
        | f -> loop (f :: accu)
        | exception End_of_file -> accu
      in
      loop []
    end

let string_of_file filename =
  let ic = open_in filename in
  Fun.protect
    ~finally:(fun () -> close_in ic)
    (fun () -> really_input_string ic (in_channel_length ic))

let string_to_file ~filename string =
  let oc = open_out filename in
  Fun.protect
    ~finally:(fun () -> close_out oc)
    (fun () -> output_string oc string)

let digit_of_char = function
  | '0'..'9' as c -> int_of_char c - int_of_char '0'
  | 'a'..'z' as c -> int_of_char c - int_of_char 'a' + 10
  | 'A'..'Z' as c -> int_of_char c - int_of_char 'A' + 10
  | _ -> 0

let z97 = Z.of_int 97
let z100 = Z.of_int 100

let get_rib_key rib =
  let extendedRib = Buffer.create (String.length rib) in
  String.iter (fun c ->
      let x = digit_of_char c in
      let x =
        if x < 10 then
          x
        else
          (x + (1 lsl ((x - 10) / 9))) mod 10
      in
      Buffer.add_string extendedRib (string_of_int x)
    ) rib;
  let extendedRibInt = Z.of_string (Buffer.contents extendedRib) in
  97 - Z.(to_int (extendedRibInt * z100 mod z97))

let wrap_exn f x =
  try Some (f x) with _ -> None
