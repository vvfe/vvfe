(**************************************************************************)
(*                                  VVFE                                  *)
(*                                                                        *)
(*  Copyright © 2021-2022 Inria                                           *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU Affero General Public License as        *)
(*  published by the Free Software Foundation, either version 3 of the    *)
(*  License, or (at your option) any later version, with the additional   *)
(*  exemption that compiling, linking, and/or using OpenSSL is allowed.   *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful, but   *)
(*  WITHOUT ANY WARRANTY; without even the implied warranty of            *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *)
(*  Affero General Public License for more details.                       *)
(*                                                                        *)
(*  You should have received a copy of the GNU Affero General Public      *)
(*  License along with this program.  If not, see                         *)
(*  <http://www.gnu.org/licenses/>.                                       *)
(**************************************************************************)

open Vvfe_platform.Platform

type 'a t = string -> int -> int * 'a

let yield () _seed state =
  state, ()

let return x _seed state =
  state, x

let bind x f seed state =
  let state, x = x seed state in
  f x seed state

let fail e _seed _state =
  raise e

let rec precise_bit_length q =
  if Z.(compare q zero = 0) then
    0
  else
    1 + precise_bit_length Z.(shift_right q 1)

let random q =
  let n = precise_bit_length q in
  if n = 0 then
    invalid_arg "argument to Prng.random is zero"
  else if n > 256 then
    invalid_arg "argument to Prng.random is too big"
  else (
    let mask = Z.(shift_left one n - one) in
    fun seed ->
    let rec loop state =
      let candidate =
        Printf.sprintf "%s%d" seed state
        |> sha256_hex
        |> Z.of_hex
        |> Z.logand mask
      in
      let state = state + 1 in
      if Z.compare candidate q < 0 then
        state, candidate
      else
        loop state
    in
    loop
  )

let run seed x =
  snd (x seed 0)
