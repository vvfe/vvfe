(**************************************************************************)
(*                                  VVFE                                  *)
(*                                                                        *)
(*  Copyright © 2012-2021 Inria                                           *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU Affero General Public License as        *)
(*  published by the Free Software Foundation, either version 3 of the    *)
(*  License, or (at your option) any later version, with the additional   *)
(*  exemption that compiling, linking, and/or using OpenSSL is allowed.   *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful, but   *)
(*  WITHOUT ANY WARRANTY; without even the implied warranty of            *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *)
(*  Affero General Public License for more details.                       *)
(*                                                                        *)
(*  You should have received a copy of the GNU Affero General Public      *)
(*  License along with this program.  If not, see                         *)
(*  <http://www.gnu.org/licenses/>.                                       *)
(**************************************************************************)

open Vvfe_platform
open Platform
open Signatures_core

val ( let@ ) : ('a -> 'b) -> 'a -> 'b

module Array : sig
  include module type of Array
  val exists : ('a -> bool) -> 'a array -> bool
  val forall : ('a -> bool) -> 'a array -> bool
  val forall2 : ('a -> 'b -> bool) -> 'a array -> 'b array -> bool
  val forall3 : ('a -> 'b -> 'c -> bool) -> 'a array -> 'b array -> 'c array -> bool
  val map2 : ('a -> 'b -> 'c) -> 'a array -> 'b array -> 'c array
  val map3 : ('a -> 'b -> 'c -> 'd) ->
             'a array -> 'b array -> 'c array -> 'd array
  val combine : 'a array -> 'b array -> ('a * 'b) array
end

module String : sig
  include module type of String
  val startswith : string -> string -> bool
end

module List : sig
  include module type of List
  val filter_map : ('a -> 'b option) -> 'a list -> 'b list
end

module Shape : sig
  type 'a t =
    | SAtomic of 'a
    | SArray of 'a t array
  val to_array : 'a t -> 'a array
  val map : ('a -> 'b) -> 'a t -> 'b t
  val map2 : ('a -> 'b -> 'c) -> 'a t -> 'b t -> 'c t
  val split : ('a * 'b) t -> 'a t * 'b t
  val combine : 'a t -> 'b t -> ('a * 'b) t
  val forall : ('a -> bool) -> 'a t -> bool
  val forall2 : ('a -> 'b -> bool) -> 'a t -> 'b t -> bool
end

module SSet : Set.S with type elt = string
module SMap : Map.S with type key = string

module ISet : Set.S with type elt = int
module IMap : Map.S with type key = int

val sqrt : Z.t -> Z.t

module BabyStepGiantStep (G : GROUP) : sig
  val log : generator:G.t -> max:Z.t -> G.t -> Z.t option
end

val split_lines : string -> string list
val replace_underscores_by_spaces : string -> string

val files_of_dir : string -> string list
val string_of_file : string -> string
val string_to_file : filename:string -> string -> unit

val get_rib_key : string -> int

val wrap_exn : ('a -> 'b) -> 'a -> 'b option
