(**************************************************************************)
(*                                  VVFE                                  *)
(*                                                                        *)
(*  Copyright © 2021-2022 Inria                                           *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU Affero General Public License as        *)
(*  published by the Free Software Foundation, either version 3 of the    *)
(*  License, or (at your option) any later version, with the additional   *)
(*  exemption that compiling, linking, and/or using OpenSSL is allowed.   *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful, but   *)
(*  WITHOUT ANY WARRANTY; without even the implied warranty of            *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *)
(*  Affero General Public License for more details.                       *)
(*                                                                        *)
(*  You should have received a copy of the GNU Affero General Public      *)
(*  License along with this program.  If not, see                         *)
(*  <http://www.gnu.org/licenses/>.                                       *)
(**************************************************************************)

open Vvfe_platform.Platform
open Serializable_j
open Common

let is_valid_reference = function
  | '&' | '0'..'9' | 'a'..'f' -> true
  | _ -> false

let is_valid_cachet = function
  | '0'..'9' | 'a'..'z' | 'A'..'Z' | '/' | '+' | '=' -> true
  | _ -> false

let normalize_input is_valid x =
  let buf = Buffer.create (String.length x) in
  String.iter (fun c ->
      if is_valid c then Buffer.add_char buf c
    ) x;
  Buffer.contents buf

let normalize_reference = normalize_input is_valid_reference
let normalize_cachet = normalize_input is_valid_cachet

module G = Vvfe_platform.Secp256r1
module S = Schnorr.Make (Deterministic_random) (G)

type path =
  [ `Root
  | `Index of int
  | `File of int * string
  ]

module type LOADER = sig
  type 'a t
  val load : path -> string t
end

let get_from_json json key =
  match wrap_exn Yojson.Safe.from_string json with
  | Some (`Assoc o) ->
     begin
       match List.assoc_opt key o with
       | Some (`String x) -> Some x
       | _ -> None
     end
  | _ -> None

type check_reference_error =
  [ `Missing
  | `Syntax of string
  | `Server of string
  ]

module Make (M : Signatures.MONAD) (L : LOADER with type 'a t := 'a M.t) = struct

  let ( let* ) = M.bind

  let ( let& ) (msg, x) f =
    if x then f () else M.return @@ Error msg

  let ( let$ ) (msg, x) f =
    match x with
    | None -> M.return @@ Error msg
    | Some x -> f x

  let check_reference ~rootfp ~operation ~tour reference =
    match String.split_on_char '&' reference with
    | [operationtour; election; hCle] ->
       let election = int_of_string election in
       let& () =
         `Syntax "operationtour",
         operationtour = Printf.sprintf "%d%d" operation tour
       in
       let& () = `Syntax "taille", String.length hCle = 66 in
       let h = String.sub hCle 0 64 in
       let cle = String.sub hCle 64 2 in
       let& () = `Syntax "clé RIB", cle = Printf.sprintf "%02d" (get_rib_key h) in
       let* root = L.load `Root in
       let& () = `Server "empreinte racine", rootfp = sha256_hex root in
       let$ indexfp = `Server "JSON racine", get_from_json root (string_of_int election) in
       let* index = L.load @@ `Index election in
       let& () = `Server "empreinte index", indexfp = sha256_hex index in
       let key = String.sub h 0 1 in
       let$ filefp = `Missing, get_from_json index key in
       let* file = L.load @@ `File (election, key) in
       let& () = `Server "empreinte fichier", filefp = sha256_hex file in
       let$ encrypted =
         `Missing,
         split_lines file
         |> List.find_map (fun line ->
                match String.split_on_char ';' line with
                | [r; encrypted; invalid] when r = reference -> Some (encrypted, invalid = "1")
                | _ -> None
              )
       in
       M.return @@ Ok encrypted
    | _ -> M.return @@ Error (`Syntax "format référence")

end

let decode_pubkey x =
  match split_lines x with
  | ["-----BEGIN_VERIFICATION_KEY-----"; x; "-----END_VERIFICATION_KEY-----"] -> x
  | _ -> invalid_arg "Cachet.decode_pubkey"

let check_inner_rib_key ~operation ~tour x =
  match String.split_on_char '|' x with
  | [optour; election; _nom; etablissement; h; cle] ->
     let@ () = fun cont ->
       let expected = Printf.sprintf "%d%d" operation tour in
       if optour = expected then
         cont ()
       else
         "operation ou tour incorrect", None
     in
     let expected = Printf.sprintf "%02d" (get_rib_key h) in
     let msg = Printf.sprintf "clé RIB interne (%s attendu)" expected in
     if cle = expected then (
       msg,
       Some (election, etablissement, h, cle)
     ) else (msg, None)
  | _ -> "format infoSU", None

let ( let& ) (msg, x) f =
  if x then f () else Error msg

let ( let$ ) (msg, x) f =
  match x with
  | None -> Error msg
  | Some x -> f x

let check_cachet ~operation ~tour ~pubkey cachet =
  let$ cachet = "Base64", wrap_exn decode_base64 cachet in
  let$ cachet = "JSON", wrap_exn cachet_of_string cachet in
  let$ election, etablissement, h, cle = check_inner_rib_key ~operation ~tour cachet.infoSU in
  let cachetBrutSU = cachet.infoSU ^ cachet.schnorr ^ replace_underscores_by_spaces cachet.publicKeySu in
  let cleCachetBrutSU = Printf.sprintf "%02d" (get_rib_key cachetBrutSU) in
  let& () =
    Printf.sprintf "clé cachet brut (%s attendu)" cleCachetBrutSU,
    cleCachetBrutSU = cachet.cleCachetBrut
  in
  let pubkey_raw = decode_pubkey cachet.publicKeySu in
  let& () = "intégrité clé publique", pubkey = pubkey_raw in
  let$ pubkey = "clé publique", wrap_exn G.of_string pubkey_raw in
  let& () = "validité clé publique", G.check pubkey in
  let msg = sha256_hex cachet.infoSU in
  let b () = S.check_signature pubkey ~msg ~signature:cachet.schnorr in
  Ok
    (
      int_of_string election,
      int_of_string etablissement,
      Printf.sprintf "%d%d&%s&%s%s" operation tour election h cle,
      b
    )
