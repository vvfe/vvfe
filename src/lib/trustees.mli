(**************************************************************************)
(*                                  VVFE                                  *)
(*                                                                        *)
(*  Copyright © 2012-2021 Inria                                           *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU Affero General Public License as        *)
(*  published by the Free Software Foundation, either version 3 of the    *)
(*  License, or (at your option) any later version, with the additional   *)
(*  exemption that compiling, linking, and/or using OpenSSL is allowed.   *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful, but   *)
(*  WITHOUT ANY WARRANTY; without even the implied warranty of            *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *)
(*  Affero General Public License for more details.                       *)
(*                                                                        *)
(*  You should have received a copy of the GNU Affero General Public      *)
(*  License along with this program.  If not, see                         *)
(*  <http://www.gnu.org/licenses/>.                                       *)
(**************************************************************************)

open Vvfe_platform.Platform
open Serializable_t
open Signatures

val string_of_combination_error : Signatures.combination_error -> string

module MakeKGShamir (G : GROUP) (M : RANDOM) : sig
  val generate : nb_trustees:int -> threshold:int -> (G.t trustees * Z.t list) M.t
end

module MakeCombinator (G : GROUP) : sig

  val check : G.t trustees -> bool
  (** Check consistency of a set of trustees. *)

  val combine_factors :
    G.t trustees ->
    (G.t -> G.t partial_decryption -> bool) ->
    G.t partial_decryption list -> (G.t shape, combination_error) result
                                                                  (** Compute synthetic decryption factors. *)

end
