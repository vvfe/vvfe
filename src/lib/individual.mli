(**************************************************************************)
(*                                  VVFE                                  *)
(*                                                                        *)
(*  Copyright © 2021-2022 Inria                                           *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU Affero General Public License as        *)
(*  published by the Free Software Foundation, either version 3 of the    *)
(*  License, or (at your option) any later version, with the additional   *)
(*  exemption that compiling, linking, and/or using OpenSSL is allowed.   *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful, but   *)
(*  WITHOUT ANY WARRANTY; without even the implied warranty of            *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *)
(*  Affero General Public License for more details.                       *)
(*                                                                        *)
(*  You should have received a copy of the GNU Affero General Public      *)
(*  License along with this program.  If not, see                         *)
(*  <http://www.gnu.org/licenses/>.                                       *)
(**************************************************************************)

val normalize_reference : string -> string
val normalize_cachet : string -> string

type path =
  [ `Root
  | `Index of int
  | `File of int * string
  ]

module type LOADER = sig
  type 'a t
  val load : path -> string t
end

type check_reference_error =
  [ `Missing
  | `Syntax of string
  | `Server of string
  ]

module Make (M : Signatures.MONAD) (L : LOADER with type 'a t := 'a M.t) : sig

  val check_reference :
    rootfp:string -> operation:int -> tour:int ->
    string -> (string * bool, check_reference_error) result M.t

end

val check_cachet :
  operation:int -> tour:int -> pubkey:string ->
  string -> (int * int * string * (unit -> bool), string) result
