(**************************************************************************)
(*                                  VVFE                                  *)
(*                                                                        *)
(*  Copyright © 2021-2022 Inria                                           *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU Affero General Public License as        *)
(*  published by the Free Software Foundation, either version 3 of the    *)
(*  License, or (at your option) any later version, with the additional   *)
(*  exemption that compiling, linking, and/or using OpenSSL is allowed.   *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful, but   *)
(*  WITHOUT ANY WARRANTY; without even the implied warranty of            *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *)
(*  Affero General Public License for more details.                       *)
(*                                                                        *)
(*  You should have received a copy of the GNU Affero General Public      *)
(*  License along with this program.  If not, see                         *)
(*  <http://www.gnu.org/licenses/>.                                       *)
(**************************************************************************)

open Vvfe_platform.Platform
open Vvfe_platform.Common
open Signatures

module type GROUP = Vvfe_platform.Signatures.GROUP with type number := Z.t

let base32_alphabet = "0123456789abcdefghijklmnopqrstuv"
let to_string_base32 = to_string_generic base32_alphabet
let of_string_base32 = of_string_generic base32_alphabet

module Make (M : RANDOM) (G : GROUP) = struct
  type privkey = Z.t
  type pubkey = G.t
  type keypair = privkey * pubkey

  let ( let* ) = M.bind

  let create_key () =
    let* x = M.random G.q in
    M.return (x, G.(g **~ x))

  let hash y u msg =
    Printf.sprintf "%s%%%s%%%s%%%s"
      (G.to_string G.g) (G.to_string y) (G.to_string u) msg
    |> sha256_hex
    |> (fun x -> Z.(erem (of_hex x) G.q))

  let create_signature (x, y) ~msg =
    let* t = M.random G.q in
    let u = G.(g **~ t) in
    let e = hash y u msg in
    let s = Z.(erem (t - e * x) G.q) in
    let e = to_string_base32 e and s = to_string_base32 s in
    M.return @@ Printf.sprintf "%s%%%s" e s

  let check_signature y ~msg ~signature =
    match String.split_on_char '%' signature with
    | [e; s] ->
       let e = of_string_base32 e and s = of_string_base32 s in
       let u = G.(g **~ s *~ y **~ e) in
       Z.(compare e (hash y u msg)) = 0
    | _ -> assert false
end
