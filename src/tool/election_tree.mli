(**************************************************************************)
(*                                  VVFE                                  *)
(*                                                                        *)
(*  Copyright © 2021-2022 Inria                                           *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU Affero General Public License as        *)
(*  published by the Free Software Foundation, either version 3 of the    *)
(*  License, or (at your option) any later version, with the additional   *)
(*  exemption that compiling, linking, and/or using OpenSSL is allowed.   *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful, but   *)
(*  WITHOUT ANY WARRANTY; without even the implied warranty of            *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *)
(*  Affero General Public License for more details.                       *)
(*                                                                        *)
(*  You should have received a copy of the GNU Affero General Public      *)
(*  License along with this program.  If not, see                         *)
(*  <http://www.gnu.org/licenses/>.                                       *)
(**************************************************************************)

open Vvfe
open Signatures
open Common
open Serializable_t
open Csvparser

val dispatch_ballots : operation:int -> tour:int -> Referentiel.t -> string -> ISet.t IMap.t
val dispatch_results : string -> string -> unit

val get_suffix : string -> string

module Make (G : GROUP) (M : RANDOM) : sig

  val dispatch_elections : G.t params IMap.t * G.t params IMap.t IMap.t -> string -> unit
  val dispatch_partial_decryptions : Referentiel.t -> string -> (int IMap.t * (int IMap.t * int IMap.t IMap.t)) option

  val compute_fingerprints : tour:int -> string -> unit
  val compute_encrypted_tally : string -> unit
  val compute_partial_decryptions : string -> unit M.t
  val compute_result : string -> unit
  val generate_report : int -> Referentiel.t -> string -> unit

  val init_random_tree_full :
    nb_trustees:int -> threshold:int ->
    Referentiel.t -> string -> unit M.t
  val generate_random_tree :
    nb_trustees:int -> threshold:int -> nb_ballots:int ->
    Referentiel.t -> string -> unit M.t
  val generate_random_ballots :
    nb_ballots:int -> string -> unit M.t

  val load_tree_as_referentiel : tour:int -> Referentiel.t -> string -> Referentiel.t

end
