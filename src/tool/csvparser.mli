(**************************************************************************)
(*                                  VVFE                                  *)
(*                                                                        *)
(*  Copyright © 2021-2022 Inria                                           *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU Affero General Public License as        *)
(*  published by the Free Software Foundation, either version 3 of the    *)
(*  License, or (at your option) any later version, with the additional   *)
(*  exemption that compiling, linking, and/or using OpenSSL is allowed.   *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful, but   *)
(*  WITHOUT ANY WARRANTY; without even the implied warranty of            *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *)
(*  Affero General Public License for more details.                       *)
(*                                                                        *)
(*  You should have received a copy of the GNU Affero General Public      *)
(*  License along with this program.  If not, see                         *)
(*  <http://www.gnu.org/licenses/>.                                       *)
(**************************************************************************)

exception Not_found of string

module College : sig
  type t =
    {
      ordre : int;
      nom : string;
    }
end

module Etablissement : sig
  type t =
    {
      ordre : int;
      nom : string;
      parent : int option;
    }
end

module Election : sig
  type t =
    {
      operationId : int;
      ordre : int;
      nom : string;
      college : int;
      etablissement : int;
      nbSiegePourvoir : int;
    }
end

module ListeCandidat : sig
  type t =
    {
      election : int;
      ordre : int;
      nom : string;
    }
end

module Candidat : sig
  type t =
    {
      election : int;
      listeCandidat : int;
      ordre : int;
      nom : string;
      prenom : string;
    }
end

module Suffrage : sig
  type t =
    {
      operationId : int;
      election : int;
      collegeId : int;
      etablissmentId : int;
      choix : string;
      empreinteSuffrage : string;
    }
end

module ClePubliqueBureau : sig
  type t =
    {
      ordre : int;
      cle_publique : string;
    }
end

module ClePubliqueMembreBureau : sig
  type t =
    {
      ordre_bv : int;
      ordre_assesseur : int;
      cle_publique : string;
    }
end

module DechiffrementPartiel : sig
  type t =
    {
      ordrebureauvote : int;
      seuil : int;
      ordreelection : int;
      ordreetablissement : int option;
      ordrecollege : int option;
      it : string;
    }
end

module Referentiel : sig
  type t =
    {
      colleges : College.t list Lazy.t;
      etablissements : Etablissement.t list Lazy.t;
      elections : Election.t list Lazy.t;
      listes : ListeCandidat.t list Lazy.t;
      candidats : Candidat.t list Lazy.t;
      suffrages : (Suffrage.t -> unit) -> unit;
      clesPubliques : ClePubliqueBureau.t list Lazy.t;
      clesMembres : ClePubliqueMembreBureau.t list Lazy.t;
      dechiffrementsPartiels : (DechiffrementPartiel.t -> unit) -> unit;
      clePubliqueSignatureSuffrage : string Lazy.t;
    }
  val load : string -> t
  val save : string -> t -> unit
end

val load_cachet_pubkey : string -> string
