(**************************************************************************)
(*                                  VVFE                                  *)
(*                                                                        *)
(*  Copyright © 2021-2022 Inria                                           *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU Affero General Public License as        *)
(*  published by the Free Software Foundation, either version 3 of the    *)
(*  License, or (at your option) any later version, with the additional   *)
(*  exemption that compiling, linking, and/or using OpenSSL is allowed.   *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful, but   *)
(*  WITHOUT ANY WARRANTY; without even the implied warranty of            *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *)
(*  Affero General Public License for more details.                       *)
(*                                                                        *)
(*  You should have received a copy of the GNU Affero General Public      *)
(*  License along with this program.  If not, see                         *)
(*  <http://www.gnu.org/licenses/>.                                       *)
(**************************************************************************)

open Vvfe
open Signatures
open Common
open Question_h_t
open Serializable_core_j
open Serializable_j

module E = Election

open Csvparser

type 'a elections = (Election.t * 'a params * (int -> 'a params)) IMap.t

let check_completeness xs =
  let rec loop i = function
    | j :: xs -> i = j.Candidat.ordre && loop (i + 1) xs
    | [] -> true
  in
  loop 1 xs

let make_uuid ~election ~etablissement =
  Printf.sprintf "00000000-0000-%04x-0000-%012x"
    etablissement.Etablissement.ordre election.Election.ordre

let make_election ?ordreetablissement m e_trustees election =
  let liste =
    match
      List.filter (fun y ->
          y.ListeCandidat.election = election.Election.ordre
        ) @@ Lazy.force m.Referentiel.listes
    with
    | [x] -> x
    | _ -> failwith "liste non unique"
  in
  let college = List.find (fun y -> y.College.ordre = election.college) @@ Lazy.force m.colleges in
  let etablissement =
    match ordreetablissement with
    | None -> List.find (fun y -> y.Etablissement.ordre = election.etablissement) @@ Lazy.force m.etablissements
    | Some z -> List.find (fun y -> y.Etablissement.ordre = z) @@ Lazy.force m.etablissements
  in
  let q_answers =
    List.filter_map (fun (y : Candidat.t) ->
        if y.election = election.ordre && y.listeCandidat = liste.ordre then
          Some y
        else None
      ) @@ Lazy.force m.Referentiel.candidats
    |> List.sort (fun a b -> Stdlib.compare a.Candidat.ordre b.Candidat.ordre)
    |> (fun ys -> assert (check_completeness ys); ys)
    |> List.map (fun (y : Candidat.t) ->
           {
             c_nom = y.nom;
             c_prenom = y.prenom;
             c_ordre = y.ordre;
           }
         )
    |> Array.of_list
  in
  let question =
    {
      q_question = liste.nom;
      q_min = 1;
      q_max = election.nbSiegePourvoir;
      q_blank = true;
      q_answers;
      q_ordre = liste.ordre;
    }
  in
  {
    e_name = election.nom;
    e_group = "Ed25519";
    e_trustees;
    e_questions = [| Homomorphic question |];
    e_uuid = make_uuid ~election ~etablissement;
    e_ordre = election.ordre;
    e_college = (college.ordre, college.nom);
    e_etablissement = (etablissement.ordre, etablissement.nom);
    e_operationId = election.operationId;
  }

let make_elections modele trustees =
  List.fold_left (fun accu x ->
      let lazy_params ordreetablissement =
        make_election ~ordreetablissement modele trustees x
      in
      IMap.add x.Election.ordre (x, make_election modele trustees x, lazy_params) accu
    ) IMap.empty @@ Lazy.force modele.elections

let has_all_indexes f xs =
  let rec loop i = function
    | [] -> true
    | x :: xs -> f x = i && loop (i + 1) xs
  in
  loop 1 xs

module MakeComputeTrustees (G : GROUP) = struct

  module C = Trustees.MakeCombinator (G)

  let compute_trustees ?seuils src =
    Lazy.force src.Referentiel.clesPubliques
    |> List.map (fun pk ->
           let ordre = pk.ClePubliqueBureau.ordre in
           let parse x = unboxed_of_string G.read x in
           let pks =
             let open ClePubliqueMembreBureau in
             Lazy.force src.Referentiel.clesMembres
             |> List.filter (fun x -> ordre = x.ordre_bv)
             |> List.sort (fun x y -> Int.compare x.ordre_assesseur y.ordre_assesseur)
             |> (fun xs -> assert (has_all_indexes (fun x -> x.ordre_assesseur) xs); xs)
             |> List.map (fun x -> parse x.cle_publique)
           in
           let trustees_threshold =
             match seuils with
             | None -> None
             | Some seuils ->
                match IMap.find_opt ordre seuils with
                | None -> Printf.ksprintf failwith "seuil not found for bureau %d" ordre
                | Some x -> Some x
           in
           let r =
             {
               trustees_overall_key = parse pk.cle_publique;
               trustees_individual_keys = pks;
               trustees_threshold;
             }
           in
           assert (C.check r);
           r
         )

end

let compute_elections src trustees elections =
  match elections with
  | Either.Left (elections, subelections) ->
     IMap.mapi (fun ordreelection ordre_bv ->
         let e = List.find (fun x -> x.Election.ordre = ordreelection) @@ Lazy.force src.Referentiel.elections in
         let t = List.nth trustees (ordre_bv - 1) in
         make_election src t e
       ) elections,
     IMap.mapi (fun ordreelection subelections ->
         let e = List.find (fun x -> x.Election.ordre = ordreelection) @@ Lazy.force src.Referentiel.elections in
         IMap.mapi (fun ordreetablissement ordre_bv ->
             let t = List.nth trustees (ordre_bv - 1) in
             make_election ~ordreetablissement src t e
           ) subelections
       ) subelections
  | Either.Right elections ->
     match trustees with
     | [t] ->
        IMap.fold (fun ordreelection _ accu ->
            let e = List.find (fun x -> x.Election.ordre = ordreelection) @@ Lazy.force src.Referentiel.elections in
            IMap.add ordreelection (make_election src t e) accu
          ) elections IMap.empty,
        IMap.fold (fun ordreelection subelections accu ->
            let e = List.find (fun x -> x.Election.ordre = ordreelection) @@ Lazy.force src.Referentiel.elections in
            let subelections =
              ISet.fold (fun ordreetablissement accu ->
                  IMap.add ordreetablissement (make_election ~ordreetablissement src t e) accu
                ) subelections IMap.empty
            in
            IMap.add ordreelection subelections accu
          ) elections IMap.empty
     | _ -> failwith "number of trustees is not 1"

type etablissement_tree =
  {
    it : Etablissement.t;
    children : int list;
  }

let compute_etablissement_forest etablissements =
  let map =
    List.fold_left (fun accu (it : Etablissement.t) ->
        IMap.add it.ordre {it; children = []} accu
      ) IMap.empty etablissements
  in
  let roots, map =
    IMap.fold (fun _ {it; _} (roots, accu) ->
        match it.parent with
        | None -> it.ordre :: roots, accu
        | Some it_parent ->
           let parent = IMap.find it_parent accu in
           let parent = {parent with children = it.ordre :: parent.children} in
           roots,
           IMap.add it_parent parent accu
      ) map ([], map)
  in
  List.sort Int.compare roots,
  IMap.map (fun x ->
      {x with children = List.sort Int.compare x.children}
    ) map

let pp_etablissement_forest fmt (roots, map) =
  let rec loop level i =
    let e = IMap.find i map in
    Format.fprintf fmt "%*s%d (%s)\n" (2 * level) "" i e.it.nom;
    List.iter (loop (level + 1)) e.children
  in
  List.iter (loop 0) roots
