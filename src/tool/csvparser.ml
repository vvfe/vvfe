(**************************************************************************)
(*                                  VVFE                                  *)
(*                                                                        *)
(*  Copyright © 2021-2022 Inria                                           *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU Affero General Public License as        *)
(*  published by the Free Software Foundation, either version 3 of the    *)
(*  License, or (at your option) any later version, with the additional   *)
(*  exemption that compiling, linking, and/or using OpenSSL is allowed.   *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful, but   *)
(*  WITHOUT ANY WARRANTY; without even the implied warranty of            *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *)
(*  Affero General Public License for more details.                       *)
(*                                                                        *)
(*  You should have received a copy of the GNU Affero General Public      *)
(*  License along with this program.  If not, see                         *)
(*  <http://www.gnu.org/licenses/>.                                       *)
(**************************************************************************)

open Vvfe_platform.Platform
open Vvfe_platform.Common
open Vvfe
open Common
open Serializable_core_j
open Serializable_j

exception Not_found of string

module G = Group.Ed25519

let to_utf8 =
  let open CamomileLibrary in
  let open CharEncoding.Configure (DefaultConfig) in
  let vvfe_encoding =
    lazy (
        match Sys.getenv_opt "VVFE_ENCODING" with
        | Some "ISO-8859-1" -> latin1
        | Some "UTF-8" | None -> utf8
        | Some x -> Printf.ksprintf failwith "unknown encoding: %s" x
      )
  in
  fun x ->
  let in_enc = Lazy.force vvfe_encoding in
  recode_string ~in_enc ~out_enc:utf8 x

let int_option_of_string x =
  if x = "" then None else Some (int_of_string x)

let string_of_int_option = function
  | None -> ""
  | Some i -> string_of_int i

type 'a item =
  {
    label : string option;
    cast : string -> 'a;
    dump : 'a -> string;
  }

type _ item_list =
  | Nil : unit item_list
  | Cons : 'a item * 'b item_list -> ('a * 'b) item_list

let nil = Nil
let ( ** ) a b = Cons (a, b)

let remove_double_double_quotes x =
  let n = String.length x in
  let buf = Buffer.create n in
  let rec loop i =
    if i < n then (
      let c = x.[i] in
      Buffer.add_char buf c;
      if c = '"' && (i + 1 < n && x.[i + 1] = '"') then
        loop (i + 2)
      else
        loop (i + 1)
    ) else Buffer.contents buf
  in
  loop 0

let id x = x
let string label = {label = Some label; cast = remove_double_double_quotes; dump = id}
let txt label = {label = Some label; cast = to_utf8; dump = id}
let int label = {label = Some label; cast = int_of_string; dump = string_of_int}
let int_opt label = {label = Some label; cast = int_option_of_string; dump = string_of_int_option}
let unit = {label = None; cast = (fun _ -> ()); dump = (fun () -> "")}

type ex_list = L : 'a item_list -> ex_list

let header_match spec header =
  let rec loop (L spec) header =
    match spec, header with
    | Nil, _ -> true
    | Cons (_, _), [] -> false
    | Cons (a, b), x :: xs ->
       (match a.label with
        | Some x' -> x = x'
        | None -> true
       ) && loop (L b) xs
  in
  loop (L spec) header

let make_header spec =
  let rec loop (L spec) =
    match spec with
    | Nil -> []
    | Cons (a, b) -> Option.value ~default:"" a.label :: loop (L b)
  in
  loop (L spec)

type 'r ex_of_csv = C : ('a -> 'r) * 'a item_list -> 'r ex_of_csv

let generic_of_csv (type r) (spec : r item_list) row =
  let rec loop (C (cont, spec) : r ex_of_csv) row : r =
    match spec, row with
    | Nil, _ -> cont ()
    | Cons (_, _), [] -> failwith "unrecognized line"
    | Cons (a, b), x :: xs ->
       let cont r = cont (a.cast x, r) in
       loop (C (cont, b)) xs
  in
  loop (C (id, spec)) row

type ex_to_csv = C : 'a item_list * 'a -> ex_to_csv

let generic_to_csv spec row =
  let rec loop (C (spec, row)) =
    match spec, row with
    | Nil, () -> []
    | Cons (a, b), (x, xs) -> a.dump x :: loop (C (b, xs))
  in
  loop (C (spec, row))

module type I = sig
  type t
  type csv
  val of_csv : csv -> t
  val to_csv : t -> csv
  val spec : csv item_list
end

module Make (I : I) = struct
  let load filename =
    match Csv.load ~separator:';' filename with
    | header :: rows when header_match I.spec header ->
       List.map (fun row -> I.of_csv (generic_of_csv I.spec row)) rows
    | _ -> failwith "unrecognized header"

  let save filename xs =
    make_header I.spec
    :: List.map (fun row -> generic_to_csv I.spec (I.to_csv row)) xs
    |> Csv.save ~separator:';' filename

  let iter filename f =
    let ic = open_in filename in
    let icsv = Csv.of_channel ~separator:';' ic in
    Fun.protect
      ~finally:(fun () -> Csv.close_in icsv; close_in ic)
      begin
        fun () ->
        if header_match I.spec (Csv.next icsv) then
          Csv.iter ~f:(fun row -> f (I.of_csv (generic_of_csv I.spec row))) icsv
        else failwith "unrecognized header"
      end

  let save_iter filename f =
    let oc = open_out filename in
    let ocsv = Csv.to_channel ~separator:';' oc in
    Fun.protect
      ~finally:(fun () -> Csv.close_out ocsv; close_out oc)
      begin
        fun () ->
        Csv.output_record ocsv (make_header I.spec);
        f (fun row -> Csv.output_record ocsv (generic_to_csv I.spec (I.to_csv row)))
      end
end

module College = struct
  type t =
    {
      ordre : int;
      nom : string;
    }
  type csv = unit * (int * (string * unit))
  let of_csv ((), (ordre, (nom, ()))) = {ordre; nom}
  let to_csv {ordre; nom} = ((), (ordre, (nom, ())))
  let spec = unit ** int "ordre" ** txt "nom" ** nil
end

module Etablissement = struct
  type t =
    {
      ordre : int;
      nom : string;
      parent : int option;
    }
  type csv = unit * (int * (string * (unit * (int option * unit))))
  let of_csv ((), (ordre, (nom, ((), (parent, ()))))) = {ordre; nom; parent}
  let to_csv {ordre; nom; parent} = ((), (ordre, (nom, ((), (parent, ())))))
  let spec = unit ** int "ordre" ** txt "nom" ** unit ** int_opt "parent" ** nil
end

module Election = struct
  type t =
    {
      operationId : int;
      ordre : int;
      nom : string;
      college : int;
      etablissement : int;
      nbSiegePourvoir : int;
    }
  type csv = int * (int * (string * (int * (int * (unit * (unit * (unit * (int * unit))))))))
  let of_csv (operationId, (ordre, (nom, (college, (etablissement, ((), ((), ((), (nbSiegePourvoir, ()))))))))) =
    {operationId; ordre; nom; college; etablissement; nbSiegePourvoir}
  let to_csv {operationId; ordre; nom; college; etablissement; nbSiegePourvoir} =
    (operationId, (ordre, (nom, (college, (etablissement, ((), ((), ((), (nbSiegePourvoir, ())))))))))
  let spec =
    int "operationId" ** int "ordre" ** txt "nom" ** int "college"
    ** int "etablissement" ** unit ** unit ** unit ** int "nbSiegePourvoir"
    ** nil
end

module ListeCandidat = struct
  type t =
    {
      election : int;
      ordre : int;
      nom : string;
    }
  type csv = unit * (int * (int * (string * unit)))
  let of_csv ((), (election, (ordre, (nom, ())))) = {election; ordre; nom}
  let to_csv {election; ordre; nom} = ((), (election, (ordre, (nom, ()))))
  let spec = unit ** int "election" ** int "ordre" ** txt "nom" ** nil
end

module Candidat = struct
  type t =
    {
      election : int;
      listeCandidat : int;
      ordre : int;
      nom : string;
      prenom : string;
    }
  type csv = unit * (int * (int * (int * (string * (string * unit)))))
  let of_csv ((), (election, (listeCandidat, (ordre, (nom, (prenom, ())))))) =
    {election; listeCandidat; ordre; nom; prenom}
  let to_csv {election; listeCandidat; ordre; nom; prenom} =
    ((), (election, (listeCandidat, (ordre, (nom, (prenom, ()))))))
  let spec =
    unit ** int "election" ** int "listeCandidat" ** int "ordre"
    ** txt "nom" ** txt "prenom" ** nil
end

module Suffrage = struct
  type t =
    {
      operationId : int;
      election : int;
      collegeId : int;
      etablissmentId : int;
      choix : string;
      empreinteSuffrage : string;
    }
  type csv = int * (int * (unit * (unit * (int * (int * (unit * (string * (unit * (unit * (string * unit))))))))))
  let of_csv (operationId, (election, ((), ((), (collegeId, (etablissmentId, ((), (choix, ((), ((), (empreinteSuffrage, ()))))))))))) =
    {operationId; election; collegeId; etablissmentId; choix; empreinteSuffrage}
  let to_csv {operationId; election; collegeId; etablissmentId; choix; empreinteSuffrage} =
    (operationId, (election, ((), ((), (collegeId, (etablissmentId, ((), (choix, ((), ((), (empreinteSuffrage, ())))))))))))
  let spec =
    int "operationid" ** int "election" ** unit ** unit ** int "collegeid"
    ** int "etablissementid" ** unit ** string "choix"
    ** unit ** unit ** string "empreintesuffrage" ** nil
end

let check_point p x y =
  let p = G.to_point p in
  Z.compare x p.x = 0 && Z.compare y p.y = 0

let parse_cle_publique data =
  let data = decode_base64 data in
  assert (String.length data = 64);
  let x = String.sub data 0 32 and y = String.sub data 32 32 in
  let x = of_string_base256 x and y = of_string_base256 y in
  let r = G.of_point {x; y} in
  assert (check_point r x y);
  string_of_unboxed G.write r

let enlarge n str =
  let prefix = String.make (n - String.length str) '\000' in
  prefix ^ str

let dump_point p =
  let {x; y} = G.to_point p in
  let x = to_string_base256 x and y = to_string_base256 y in
  enlarge 32 x ^ enlarge 32 y

let dump_cle_publique p =
  let p = unboxed_of_string G.read p in
  encode_base64 (dump_point p)

let cle_publique =
  {
    label = Some "cle_publique";
    cast = parse_cle_publique;
    dump = dump_cle_publique;
  }

module ClePubliqueBureau = struct
  type t =
    {
      ordre : int;
      cle_publique : string;
    }
  type csv = int * (string * unit)
  let of_csv (ordre, (cle_publique, ())) = {ordre; cle_publique}
  let to_csv {ordre; cle_publique} = (ordre, (cle_publique, ()))
  let spec = int "ordre" ** cle_publique ** nil
end

module ClePubliqueMembreBureau = struct
  type t =
    {
      ordre_bv : int;
      ordre_assesseur : int;
      cle_publique : string;
    }
  type csv = int * (int * (string * unit))
  let of_csv (ordre_bv, (ordre_assesseur, (cle_publique, ()))) =
    {ordre_bv; ordre_assesseur; cle_publique}
  let to_csv {ordre_bv; ordre_assesseur; cle_publique} =
    (ordre_bv, (ordre_assesseur, (cle_publique, ())))
  let spec =
    int "ordre_bv" ** int "ordre_assesseur" ** cle_publique ** nil
end

let get_numbers data =
  let n = String.length data in
  assert (n mod 32 = 0);
  let rec loop i accu =
    if i < n then
      loop (i + 32) (of_string_base256 (String.sub data i 32) :: accu)
    else
      List.rev accu
  in
  loop 0 []

let get_partial_decryption_items zs =
  let rec loop accu = function
    | x1 :: y1 :: x2 :: y2 :: challenge :: response :: zs ->
       let product_alpha = G.of_point {x = x1; y = y1} in
       assert (check_point product_alpha x1 y1);
       let factor = G.of_point {x = x2; y = y2} in
       assert (check_point factor x2 y2);
       let proof = {challenge; response} in
       loop ({product_alpha; factor; proof} :: accu) zs
    | [] -> List.rev accu
    | _ -> failwith "wrong number of numbers in partial decryption"
  in
  loop [] zs

let get_partial_decryption data =
  decode_base64 data
  |> get_numbers
  |> get_partial_decryption_items
  |> List.map (fun x -> Shape.SAtomic x)
  (* we assume a single question *)
  |> (fun x -> Shape.SArray [|Shape.SArray (Array.of_list x)|])

let dump_proof {challenge; response} =
  let challenge = to_string_base256 challenge in
  let response = to_string_base256 response in
  enlarge 32 challenge ^ enlarge 32 response

let dump_partial_decryption xs =
  let buf = Buffer.create 1024 in
  let rec loop = function
    | Shape.SArray xs -> Array.iter loop xs
    | Shape.SAtomic (x : G.t partial_decryption_item) ->
       let {product_alpha; factor; proof} = x in
       Buffer.add_string buf (dump_point product_alpha);
       Buffer.add_string buf (dump_point factor);
       Buffer.add_string buf (dump_proof proof)
  in
  loop xs;
  encode_base64 (Buffer.contents buf)

module DechiffrementPartiel = struct
  type t =
    {
      ordrebureauvote : int;
      seuil : int;
      ordreelection : int;
      ordreetablissement : int option;
      ordrecollege : int option;
      it : string;
    }
  type csv = int * (int * (int * (int option * (int option * (int * (string * unit))))))
  let of_csv (ordrebureauvote, (seuil, (ordreelection, (ordreetablissement, (ordrecollege, (ordredechiffrement, (serializedencodedpart, ()))))))) =
    let pd_index = ordredechiffrement in
    let pd_items = get_partial_decryption serializedencodedpart in
    {
      ordrebureauvote; seuil; ordreelection; ordreetablissement; ordrecollege;
      it = string_of_partial_decryption G.write {pd_index; pd_items};
    }
  let to_csv {ordrebureauvote; seuil; ordreelection; ordreetablissement; ordrecollege; it} =
    let {pd_index; pd_items} = partial_decryption_of_string G.read it in
    let ordredechiffrement = pd_index in
    let serializedencodedpart = dump_partial_decryption pd_items in
    (ordrebureauvote, (seuil, (ordreelection, (ordreetablissement, (ordrecollege, (ordredechiffrement, (serializedencodedpart, ())))))))
  let spec =
    int "ordrebureauvote" ** int "seuil" ** int "ordreelection"
    ** int_opt "ordreetablissement" ** int_opt "ordrecollege"
    ** int "ordredechiffrement" ** string "serializedencodedpart" ** nil
end

let find_file_opt ?(suffix = ".csv") files prefix =
  List.find_opt
    (fun x ->
      Filename.check_suffix x suffix && String.startswith x prefix
    ) files

let load_cachet_pubkey filename =
  match split_lines (string_of_file filename) with
  | ["-----BEGIN VERIFICATION KEY-----"; x; "-----END VERIFICATION KEY-----"] -> x
  | _ -> invalid_arg "Csvparser.load_cachet_pubkey"

let save_cachet_pubkey filename pubkey =
  Printf.sprintf
    "-----BEGIN VERIFICATION KEY-----\n%s\n-----END VERIFICATION KEY-----\n"
    pubkey
  |> string_to_file ~filename

module Referentiel = struct
  type t =
    {
      colleges : College.t list Lazy.t;
      etablissements : Etablissement.t list Lazy.t;
      elections : Election.t list Lazy.t;
      listes : ListeCandidat.t list Lazy.t;
      candidats : Candidat.t list Lazy.t;
      suffrages : (Suffrage.t -> unit) -> unit;
      clesPubliques : ClePubliqueBureau.t list Lazy.t;
      clesMembres : ClePubliqueMembreBureau.t list Lazy.t;
      dechiffrementsPartiels : (DechiffrementPartiel.t -> unit) -> unit;
      clePubliqueSignatureSuffrage : string Lazy.t;
    }

  module College = Make (College)
  module Etablissement = Make (Etablissement)
  module Election = Make (Election)
  module ListeCandidat = Make (ListeCandidat)
  module Candidat = Make (Candidat)
  module Suffrage = Make (Suffrage)
  module ClePubliqueBureau = Make (ClePubliqueBureau)
  module ClePubliqueMembreBureau = Make (ClePubliqueMembreBureau)
  module DechiffrementPartiel = Make (DechiffrementPartiel)

  let load dirname =
    let files = files_of_dir dirname in
    let l ?suffix load prefix =
      match find_file_opt ?suffix files prefix with
      | None -> raise (Not_found prefix)
      | Some f -> load (Filename.concat dirname f)
    in
    {
      colleges = lazy (l College.load "CO_");
      etablissements = lazy (l Etablissement.load "ET_");
      elections = lazy (l Election.load "EN_");
      listes = lazy (l ListeCandidat.load "LC_");
      candidats = lazy (l Candidat.load "CA_");
      suffrages = (fun f -> l (fun filename -> Suffrage.iter filename f) "R_SU_CL_");
      clesPubliques = lazy (l ClePubliqueBureau.load "CLE_PUBLIQUE_BUREAU_VOTE_");
      clesMembres = lazy (l ClePubliqueMembreBureau.load "CLES_PUBLIQUES_MEMBRES_BUREAU_VOTE_");
      dechiffrementsPartiels = (fun f -> l (fun filename -> DechiffrementPartiel.iter filename f) "DECHIFFREMENT_PARTIEL_");
      clePubliqueSignatureSuffrage = lazy (l ~suffix:".pem" load_cachet_pubkey "CLE_PUBLIQUE_SIGNATURE_SUFFRAGE_");
    }

  let save dirname r =
    let s ?(suffix = ".csv") save prefix =
      let filename = prefix ^ suffix in
      save (Filename.concat dirname filename)
    in
    s College.save "CO_" @@ Lazy.force r.colleges;
    s Etablissement.save "ET_" @@ Lazy.force r.etablissements;
    s Election.save "EN_" @@ Lazy.force r.elections;
    s ListeCandidat.save "LC_" @@ Lazy.force r.listes;
    s Candidat.save "CA_" @@ Lazy.force r.candidats;
    s Suffrage.save_iter "R_SU_CL_" r.suffrages;
    s ClePubliqueBureau.save "CLE_PUBLIQUE_BUREAU_VOTE_" @@ Lazy.force r.clesPubliques;
    s ClePubliqueMembreBureau.save "CLES_PUBLIQUES_MEMBRES_BUREAU_VOTE_" @@ Lazy.force r.clesMembres;
    s DechiffrementPartiel.save_iter "DECHIFFREMENT_PARTIEL_" r.dechiffrementsPartiels;
    s ~suffix:".pem" save_cachet_pubkey "CLE_PUBLIQUE_SIGNATURE_SUFFRAGE_" @@ Lazy.force r.clePubliqueSignatureSuffrage
end
