(**************************************************************************)
(*                                  VVFE                                  *)
(*                                                                        *)
(*  Copyright © 2021-2022 Inria                                           *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU Affero General Public License as        *)
(*  published by the Free Software Foundation, either version 3 of the    *)
(*  License, or (at your option) any later version, with the additional   *)
(*  exemption that compiling, linking, and/or using OpenSSL is allowed.   *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful, but   *)
(*  WITHOUT ANY WARRANTY; without even the implied warranty of            *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *)
(*  Affero General Public License for more details.                       *)
(*                                                                        *)
(*  You should have received a copy of the GNU Affero General Public      *)
(*  License along with this program.  If not, see                         *)
(*  <http://www.gnu.org/licenses/>.                                       *)
(**************************************************************************)

open Vvfe_platform.Platform
open Vvfe
open Common
open Signatures
open Serializable_builtin_t
open Serializable_core_j
open Serializable_j
open Question_h_t

module E = Election

open Csvparser

let ( / ) = Filename.concat

let utf8_length s =
  Uutf.String.fold_utf_8 (fun accu _ c ->
      match c with
      | `Uchar c -> accu + Uucp.Break.tty_width_hint c
      | _ -> accu
    ) 0 s

let sort_key =
  let open CamomileLibrary in
  let module X = UCol.Make (DefaultConfig) (UTF8) in
  X.sort_key

let ensure_dir dirname =
  let open Unix in
  try mkdir dirname 0o755
  with Unix_error(EEXIST, _, _) -> ()

let open_file dirname filename files election etablissmentId =
  ensure_dir dirname;
  let ( // ) dirname i =
    let dirname = dirname / string_of_int i in
    ensure_dir dirname;
    dirname
  in
  let dirname = dirname // election in
  let oc_ref, etablissements =
    match Hashtbl.find_opt files election with
    | Some z -> z
    | None ->
       let z = ref None, Hashtbl.create 16 in
       Hashtbl.add files election z;
       z
  in
  match etablissmentId with
  | None ->
     begin
       match !oc_ref with
       | Some oc -> oc
       | None ->
          let oc = open_out (dirname / filename) in
          oc_ref := Some oc;
          oc
     end
  | Some etablissmentId ->
     begin
       let dirname = dirname // etablissmentId in
       match Hashtbl.find_opt etablissements etablissmentId with
       | Some oc -> oc
       | None ->
          let oc = open_out (dirname / filename) in
          Hashtbl.add etablissements etablissmentId oc;
          oc
     end

let close_files files =
  Hashtbl.iter (fun _ (oc_ref, etablissements) ->
      Option.iter close_out !oc_ref;
      Hashtbl.iter (fun _ oc ->
          close_out oc
        ) etablissements
    ) files;
  Hashtbl.clear files

let dispatch_ballots ~operation ~tour src dst =
  let open Suffrage in
  let files = Hashtbl.create 16 in
  let elections = ref IMap.empty in
  src.Referentiel.suffrages (fun x ->
      let election =
        List.find (fun y -> y.Election.ordre = x.election) @@ Lazy.force src.elections
      in
      assert (election.operationId = operation);
      let empreinte =
        Printf.sprintf "%s%d%d%d%d"
          x.choix x.operationId tour x.election x.etablissmentId
        |> sha256_hex
      in
      assert (election.operationId = x.operationId);
      assert (election.college = x.collegeId);
      assert (x.etablissmentId >= 0);
      let ( =% ) x y =
        x = y || (Printf.eprintf "Incohérence d'empreinte:\n-%s\n+%s\n" x y; true)
      in
      assert (x.empreinteSuffrage = "" || empreinte =% x.empreinteSuffrage);
      let cur = Option.value ~default:ISet.empty (IMap.find_opt x.election !elections) in
      elections := IMap.add x.election (ISet.add x.etablissmentId cur) !elections;
      let oc =
        open_file dst "ballots.jsons" files x.election (Some x.etablissmentId)
      in
      output_string oc x.choix;
      output_string oc "\n"
    );
  close_files files;
  !elections

let string_of_lines lines =
  let buf = Buffer.create 1024 in
  List.iter (fun line ->
      Buffer.add_string buf line;
      Buffer.add_char buf '\n'
    ) lines;
  Buffer.contents buf

let iter_on_lines_of_file f filename =
  let ic = open_in filename in
  Fun.protect
    ~finally:(fun () -> close_in ic)
    begin
      fun () ->
      let rec loop () =
        match input_line ic with
        | line -> f line; loop ()
        | exception End_of_file -> ()
      in
      loop ()
    end

let sset_to_file filename sset =
  let oc = open_out filename in
  Fun.protect
    ~finally:(fun () -> close_out oc)
    (fun () -> SSet.iter (fun x -> Printf.fprintf oc "%s\n" x) sset)

let dispatch_results src dst =
  ensure_dir dst;
  files_of_dir src
  |> List.filter_map int_of_string_opt
  |> List.sort Int.compare
  |> List.map (fun election ->
         let election_str = string_of_int election in
         let src = src / election_str in
         let dst = dst / election_str in
         let copy f = Unix.link (src / f) (dst / f) in
         ensure_dir dst;
         copy "result.md";
         let subfiles = ref SMap.empty in
         iter_on_lines_of_file (fun line ->
             let key =
               match String.split_on_char '&' line with
               | [_operationTour; _election; h] -> String.sub h 0 1
               | _ -> failwith "ill-formed reference"
             in
             let cur = Option.value ~default:[] (SMap.find_opt key !subfiles) in
             subfiles := SMap.add key (line :: cur) !subfiles
           ) (src / "fingerprints.txt");
         let tree =
           SMap.mapi (fun key lines ->
               let lines = string_of_lines (List.rev lines) in
               let filename = dst / Printf.sprintf "fingerprints-%s.txt" key in
               string_to_file ~filename lines;
               sha256_hex lines
             ) !subfiles
         in
         let json =
           SMap.bindings tree
           |> List.map (fun (key, h) -> (key, `String h))
           |> (fun x -> `Assoc x)
           |> Yojson.Safe.to_string
         in
         let filename = dst / "fingerprints-index.json" in
         string_to_file ~filename json;
         election_str, `String (sha256_hex json)
       )
  |> (fun json ->
    let json = `Assoc json in
    Yojson.Safe.to_file (dst / "fingerprints-root.json") json
  )

let dir_sep =
  assert (String.length Filename.dir_sep = 1);
  Filename.dir_sep.[0]

let rec remove_empty = function
  | "" :: xs -> remove_empty xs
  | xs -> xs

let list_of_path x =
  remove_empty (List.rev (String.split_on_char dir_sep x))

let path_of_list x =
  String.concat Filename.dir_sep (List.rev x)

let get_suffix dir =
  let is_root dir =
    let dir = path_of_list dir in
    Sys.file_exists (dir / "trustees.jsons")
  in
  match list_of_path dir with
  | election :: dir when is_root dir -> election
  | etablissement :: election :: dir when is_root dir ->
     Printf.sprintf "%s/%s" election etablissement
  | _ -> failwith "could not find trustees.jsons"


module Make (G : GROUP) (M : RANDOM) = struct

  let ( let* ) = M.bind

  module type ELECTION = ELECTION with type 'a m = 'a M.t

  let dispatch_partial_decryptions src dst =
    let open DechiffrementPartiel in
    let files = Hashtbl.create 16 in
    let seuils = ref IMap.empty in
    let elections = ref IMap.empty in
    let subelections = ref IMap.empty in
    let@ () = fun cont ->
      try
        Fun.protect
          ~finally:(fun () -> close_files files)
          (fun () -> Some (cont ()))
      with Csvparser.Not_found _ -> None
    in
    src.Referentiel.dechiffrementsPartiels (fun x ->
        let election =
          List.find
            (fun y -> y.Election.ordre = x.ordreelection) @@ Lazy.force src.elections
        in
        assert (
            match x.ordrecollege, x.ordreetablissement with
            | None, None -> true
            | Some x, Some _ -> x = election.college
            | _, _ -> false
          );
        let oc =
          open_file dst "partial_decryptions.jsons" files
            x.ordreelection x.ordreetablissement
        in
        let b =
          match IMap.find_opt x.ordrebureauvote !seuils with
          | None -> seuils := IMap.add x.ordrebureauvote x.seuil !seuils; true
          | Some s -> s = x.seuil
        in
        assert b;
        let b =
          match x.ordreetablissement with
          | None ->
             begin
               match IMap.find_opt x.ordreelection !elections with
               | None ->
                  elections := IMap.add x.ordreelection x.ordrebureauvote !elections;
                  true
               | Some e -> e = x.ordrebureauvote
             end
          | Some ordreetablissement ->
             begin
               let cur =
                 Option.value ~default:IMap.empty
                   (IMap.find_opt x.ordreelection !subelections)
               in
               match IMap.find_opt ordreetablissement cur with
               | None ->
                  subelections :=
                    IMap.add x.ordreelection
                      (IMap.add ordreetablissement x.ordrebureauvote cur)
                      !subelections;
                  true
               | Some e -> e = x.ordrebureauvote
             end
        in
        assert b;
        output_string oc x.it;
        output_string oc "\n";
      );
    !seuils, (!elections, !subelections)

  let dispatch_elections (elections, subelections) dst =
    let ( / ) = Filename.concat in
    IMap.iter (fun i election ->
        let filename = dst / string_of_int i / "election.json" in
        let oc = open_out filename in
        output_string oc (string_of_params G.write election);
        close_out oc
      ) elections;
    IMap.iter (fun i subelections ->
        IMap.iter (fun j election ->
            let filename = dst / string_of_int i / string_of_int j / "election.json" in
            let oc = open_out filename in
            output_string oc (string_of_params G.write election);
            close_out oc
          ) subelections
      ) subelections

  let find_election_json dir =
    let file = dir / "election.json" in
    if not @@ Sys.file_exists file then failwith "could not find election.json";
    let mode =
      match list_of_path dir with
      | etablissement :: dir ->
         let dir = path_of_list dir in
         let file = dir / "election.json" in
         if Sys.file_exists file then
           `SubElection (int_of_string etablissement)
         else
           `Election
      | [] -> `Election
    in
    let module R =
      struct
        let raw_election = string_of_file file
      end
    in
    let module E = E.Make (R) (M) () in
    mode, (module E : ELECTION)

  let compute_fingerprints ~tour dir =
    let mode, election = find_election_json dir in
    let module E = (val election) in
    match mode with
    | `SubElection etablissement ->
       let invalid_fingerprints =
         let res = ref SSet.empty in
         iter_on_lines_of_file (fun hash ->
             res := SSet.add hash !res
           ) (dir / "invalid.txt");
         !res
       in
       let fingerprints = ref SSet.empty in
       iter_on_lines_of_file (fun ballot ->
           let h =
             Printf.sprintf "%s%d%d%d%d"
               ballot E.election.e_operationId tour E.election.e_ordre etablissement
             |> sha256_hex
           in
           let reference =
             Printf.sprintf "%d%d&%d&%s%02d"
               E.election.e_operationId tour E.election.e_ordre h (get_rib_key h)
           in
           let raw_fingerprint = sha256_hex ballot in
           let invalid = SSet.mem raw_fingerprint invalid_fingerprints in
           let fingerprint =
             Printf.sprintf "%s;%s;%d"
               reference raw_fingerprint (if invalid then 1 else 0)
           in
           fingerprints := SSet.add fingerprint !fingerprints
         ) (dir / "ballots.jsons");
       sset_to_file (dir / "fingerprints.txt") !fingerprints
    | `Election ->
       let fingerprints = ref SSet.empty in
       let () =
         files_of_dir dir
         |> List.filter_map int_of_string_opt
         |> List.iter (fun etablissement ->
                let dir = dir / string_of_int etablissement in
                iter_on_lines_of_file (fun fingerprint ->
                    fingerprints := SSet.add fingerprint !fingerprints
                  ) (dir / "fingerprints.txt")
              )
       in
       sset_to_file (dir / "fingerprints.txt") !fingerprints

  let compute_encrypted_tally dir =
    let mode, election = find_election_json dir in
    let module E = (val election) in
    let encrypted_tally =
      match mode with
      | `SubElection _ ->
         let ballots_filename = dir / "ballots.jsons" in
         let result = ref E.E.neutral_ciphertexts in
         let length = ref 0 and invalid = ref 0 in
         let oc_invalid = open_out (dir / "invalid.txt") in
         iter_on_lines_of_file (fun ballot_as_string ->
             let ballot = E.ballot_of_string ballot_as_string in
             if false && ballot_as_string <> E.string_of_ballot ballot then (
               let fname = Filename.temp_file "vvfe-ballots-" ".jsons" in
               let oc = open_out fname in
               output_string oc ballot_as_string;
               output_string oc "\n";
               output_string oc (E.string_of_ballot ballot);
               output_string oc "\n";
               close_out oc;
               Printf.eprintf "Ballot not in canonical form saved in %s\n%!" fname
             );
             if E.E.check_ballot ballot then (
               let ciphertexts = E.E.extract_ciphertexts ballot in
               result := E.E.combine_ciphertexts !result ciphertexts;
               length := !length + 1
             ) else (
               Printf.fprintf oc_invalid "%s\n" (sha256_hex ballot_as_string);
               invalid := !invalid + 1
             )
           ) ballots_filename;
         close_out oc_invalid;
         {et_length = !length; et_invalid = !invalid; et_product = !result}
      | `Election ->
         files_of_dir dir
         |> List.filter_map int_of_string_opt
         |> List.fold_left (fun ({et_length; et_invalid; et_product} as accu) etablissement ->
                let dir = dir / string_of_int etablissement in
                let file = dir / "encrypted_tally.json" in
                if Sys.file_exists file then (
                  let sub_encrypted_tally =
                    string_of_file file
                    |> encrypted_tally_of_string E.G.read
                  in
                  {
                    et_length = et_length + sub_encrypted_tally.et_length;
                    et_invalid = et_invalid + sub_encrypted_tally.et_invalid;
                    et_product = E.E.combine_ciphertexts et_product sub_encrypted_tally.et_product;
                  }
                ) else accu
              ) {et_length = 0; et_invalid = 0; et_product = E.E.neutral_ciphertexts}
    in
    let filename = dir / "encrypted_tally.json" in
    string_to_file ~filename (string_of_encrypted_tally E.G.write encrypted_tally)

  let compute_result dir =
    let _, election = find_election_json dir in
    let module E = (val election) in
    let partial_decryptions =
      let filename = dir / "partial_decryptions.jsons" in
      let result = ref [] in
      iter_on_lines_of_file (fun pd ->
          result := partial_decryption_of_string E.G.read pd :: !result
        ) filename;
      !result
    in
    let encrypted_tally =
      let filename = dir / "encrypted_tally.json" in
      string_of_file filename |> encrypted_tally_of_string E.G.read
    in
    match E.E.compute_result encrypted_tally partial_decryptions with
    | Ok result ->
       assert (E.E.check_result E.election.e_trustees result);
       let filename = dir / "result.json" in
       string_to_file ~filename (string_of_election_result E.G.write E.write_result result)
    | Error e ->
       failwith (Trustees.string_of_combination_error e)

  let generate_report tour referentiel dir =
    let mode, election = find_election_json dir in
    let module E = (val election) in
    let etablissement =
      match mode with
      | `Election -> E.election.e_etablissement
      | `SubElection _ ->
         match list_of_path dir with
         | ordre :: _ ->
            let ordre = int_of_string ordre in
            ordre,
            let et =
              List.find (fun x -> x.Etablissement.ordre = ordre)
              @@ Lazy.force referentiel.Referentiel.etablissements
            in
            et.nom
         | _ -> failwith "could not determine etablissement"
    in
    assert (Array.length E.election.e_questions = 1);
    let Homomorphic q = E.election.e_questions.(0) in
    assert q.q_blank;
    let result =
      dir / "result.json"
      |> string_of_file
      |> election_result_of_string E.G.read E.read_result
    in
    let fingerprints =
      dir / "fingerprints.txt"
      |> string_of_file
      |> sha256_hex
    in
    let num_blank, candidats =
      match
        match json_of_question_result (result.result :> raw_result).(0) with
        | `List xs ->
           List.map
             (function
              | `Int i -> i
              | _ -> failwith "generate_report: int expected"
             ) xs
        | _ -> failwith "generate_report: list expected"
      with
      | [] -> failwith "generate_report: empty result"
      | b :: c ->
         b,
         List.map2 (fun n candidat -> n, candidat)
           c (Array.to_list q.q_answers)
         |> List.sort
              (fun (_, aa) (_, bb) ->
                Stdlib.compare
                  (sort_key aa.c_nom, sort_key aa.c_prenom, aa.c_ordre)
                  (sort_key bb.c_nom, sort_key bb.c_prenom, bb.c_ordre)
              )
         |> List.map (fun (a, aa) ->
                a,
                Printf.sprintf "%s %s" aa.c_nom aa.c_prenom
              )
    in
    let num_tallied = result.num_tallied in
    let num_invalid = result.num_invalid in
    let oc = open_out (dir / "result.md") in
    Printf.fprintf oc "## %s\n" E.election.e_name;
    Printf.fprintf oc "\n";
    Printf.fprintf oc "* Scrutin : tour n°%d\n" tour;
    let () =
      match mode with
      | `Election -> ()
      | `SubElection _ ->
         Printf.fprintf oc "* Nom du collège : %s (%d)\n"
           (snd E.election.e_college) (fst E.election.e_college);
         Printf.fprintf oc "* Nom de l'établissement : %s (%d)\n"
           (snd etablissement) (fst etablissement);
         Printf.fprintf oc "* Liste : %s (%d)\n" q.q_question q.q_ordre;
    in
    Printf.fprintf oc "\n";
    assert (q.q_max = 1);
    Printf.fprintf oc "* Nombre de votants (V) :                  %9d\n" (num_tallied + num_invalid);
    Printf.fprintf oc "* Suffrages nuls (N) :                     %9d\n" num_invalid;
    Printf.fprintf oc "* Suffrages blancs (B) :                   %9d\n" num_blank;
    Printf.fprintf oc "* Suffrages valablement exprimés (V-N-B) : %9d\n" (num_tallied - num_blank);
    Printf.fprintf oc "\n";
    Printf.fprintf oc "* Empreinte de la liste des bulletins : %s\n" fingerprints;
    Printf.fprintf oc "\n";
    let header = "Candidat·e" in
    let header_size = utf8_length header in
    let max_length =
      List.fold_left (fun accu (_, s) -> max accu (utf8_length s))
        header_size candidats
    in
    Printf.fprintf oc "| %s%*s | Suffrages |\n"
      header (max_length - header_size) "";
    Printf.fprintf oc "|:%s|----------:|\n" (String.make (max_length + 1) '-');
    List.iter (fun (nb, candidat) ->
        Printf.fprintf oc "| %s%*s | %9d |\n"
          candidat (max_length - utf8_length candidat) "" nb
      ) candidats;
    close_out oc

  module T = Trustees.MakeKGShamir (G) (M)
  module C = Trustees.MakeCombinator (G)

  let array_sample a =
    let* i = M.random (Z.of_int (Array.length a)) in
    M.return a.(Z.to_int i)

  let list_sample l =
    array_sample (Array.of_list l)

  let list_sample_many n l =
    let tmp = Array.of_list l in
    let rec loop n accu =
      if n > 0 then (
        let* j = M.random (Z.of_int n) in
        let j = Z.to_int j in
        let r = tmp.(j) in
        tmp.(j) <- tmp.(n - 1);
        loop (n - 1) (r :: accu)
      ) else M.return accu
    in
    loop n []

  let rec sample_sub_election elections etablissements_map =
    let* election = list_sample elections in
    let etablissements =
      (IMap.find election.Election.etablissement etablissements_map).Analysis.children
      |> List.map (fun i -> IMap.find i etablissements_map)
    in
    if List.for_all (fun x -> x.Analysis.children = []) etablissements then (
      let* etablissement = list_sample etablissements in
      M.return (election, etablissement.it.ordre)
    ) else (
      (* assumption: this doesn't happen often *)
      sample_sub_election elections etablissements_map
    )

  let sample_plaintext params =
    let n = Array.length params.e_questions in
    let result = Array.make n [||] in
    let rec loop i =
      if i < n then (
        let Homomorphic q = params.e_questions.(i) in
        let n = Question_h.question_length q in
        let* choice = M.random (Z.of_int n) in
        let choice = Z.to_int choice in
        result.(i) <- Array.init n (fun i -> if i = choice then 1 else 0);
        loop (i + 1)
      ) else M.return result
    in
    loop 0

  let init_random_tree ~nb_trustees ~threshold referentiel dir =
    assert (1 <= threshold && threshold <= nb_trustees);
    ensure_dir dir;
    let* trustees, private_keys = T.generate ~nb_trustees ~threshold in
    assert (C.check trustees);
    string_to_file ~filename:(dir / "trustees.jsons") (string_of_trustees G.write trustees ^ "\n");
    let () =
      let oc = open_out (dir / "private_keys.jsons") in
      List.iter (fun x ->
          output_string oc (string_of_unboxed write_number x);
          output_string oc "\n"
        ) private_keys;
      close_out oc
    in
    let elections = Lazy.force referentiel.Referentiel.elections in
    let elections_map =
      Analysis.make_elections referentiel trustees
      |> IMap.mapi (fun i (_, x, lazy_params) ->
             let dir = dir / string_of_int i in
             let make dir x =
               let raw_election = string_of_params G.write x in
               ensure_dir dir;
               let filename = dir / "election.json" in
               if not @@ Sys.file_exists filename then
                 string_to_file ~filename raw_election;
               let module R = struct let raw_election = raw_election end in
               let module E = E.Make (R) (M) () in
               (module E : ELECTION)
             in
             make dir x,
             (fun j -> make (dir / string_of_int j) (lazy_params j))
           )
    in
    let roots, etablissements_map = Analysis.compute_etablissement_forest @@ Lazy.force referentiel.etablissements in
    assert (match roots with [_] -> true | _ -> false);
    M.return (elections, elections_map, etablissements_map)

  let init_random_tree_full ~nb_trustees ~threshold referentiel dir =
    let* elections, _, etablissements_map =
      init_random_tree ~nb_trustees ~threshold referentiel dir
    in
    List.iter (fun (e : Election.t) ->
        let dir = dir / string_of_int e.ordre in
        ensure_dir dir;
        let children = (IMap.find e.etablissement etablissements_map).children in
        if List.for_all (fun e -> (IMap.find e etablissements_map).children = []) children then
          List.iter (fun x -> ensure_dir (dir / string_of_int x)) children
      ) elections;
    M.return ()

  let generate_random_tree ~nb_trustees ~threshold ~nb_ballots referentiel dir =
    let* elections, elections_map, etablissements_map =
      init_random_tree ~nb_trustees ~threshold referentiel dir
    in
    let files = Hashtbl.create 16 in
    let rec generate_ballots n =
      if n > 0 then (
        let* election, etablissement = sample_sub_election elections etablissements_map in
        let _, lazy_e = IMap.find election.ordre elections_map in
        let module E = (val lazy_e etablissement) in
        let oc = open_file dir "ballots.jsons" files election.ordre (Some etablissement) in
        let* plaintext = sample_plaintext E.election in
        let* ballot = E.E.create_ballot plaintext in
        output_string oc (E.string_of_ballot ballot);
        output_string oc "\n";
        generate_ballots (n - 1)
      ) else M.return ()
    in
    let* () = generate_ballots nb_ballots in
    close_files files;
    M.return ()

  let generate_random_ballots ~nb_ballots dir =
    let _, election = find_election_json dir in
    let module E = (val election) in
    let oc = open_out (dir / "ballots.jsons") in
    let rec loop n =
      if n > 0 then (
        let* plaintext = sample_plaintext E.election in
        let* ballot = E.E.create_ballot plaintext in
        output_string oc (E.string_of_ballot ballot);
        output_string oc "\n";
        loop (n - 1)
      ) else M.return ()
    in
    let* () = loop nb_ballots in
    close_out oc;
    M.return ()

  let compute_partial_decryptions dir =
    let mode, election = find_election_json dir in
    let module E = (val election) in
    let threshold =
      match E.election.e_trustees.trustees_threshold with
      | None -> failwith "missing threshold in compute_partial_decryptions"
      | Some t -> t
    in
    let private_keys =
      let dir =
        let dir = Filename.(concat dir parent_dir_name) in
        match mode with
        | `Election -> dir
        | `SubElection _ -> Filename.(concat dir parent_dir_name)
      in
      let result = ref [] and i = ref 1 in
      iter_on_lines_of_file (fun x ->
          result := (!i, unboxed_of_string read_number x) :: !result;
          incr i
        ) (dir / "private_keys.jsons");
      List.rev !result
    in
    let et =
      dir / "encrypted_tally.json"
      |> string_of_file
      |> encrypted_tally_of_string E.G.read
    in
    let* private_keys = list_sample_many threshold private_keys in
    let rec loop accu = function
      | [] -> M.return accu
      | (index, x) :: xs ->
         let* factor = E.E.compute_factor ~index et.et_product x in
         loop (factor :: accu) xs
    in
    let* factors = loop [] private_keys in
    let () =
      let oc = open_out (dir / "partial_decryptions.jsons") in
      List.iter (fun x ->
          output_string oc (string_of_partial_decryption E.G.write x);
          output_string oc "\n"
        ) factors;
      close_out oc
    in
    M.return ()

  let load_tree_as_referentiel ~tour referentiel dirname =
    let elections = Lazy.force referentiel.Referentiel.elections in
    let etablissements = Lazy.force referentiel.etablissements in
    let _, forest = Analysis.compute_etablissement_forest etablissements in
    let suffrages f =
      List.iter (fun (e : Election.t) ->
          let dirname = dirname / string_of_int e.ordre in
          let rec visit et =
            let children = (IMap.find et forest).Analysis.children in
            if children = [] then (
              let dirname = dirname / string_of_int et in
              let filename = dirname / "ballots.jsons" in
              if Sys.file_exists filename then (
                iter_on_lines_of_file (fun choix ->
                    let empreinteSuffrage =
                      Printf.ksprintf sha256_hex "%s%d%d%d%d"
                        choix e.operationId tour e.ordre et
                    in
                    let open Suffrage in
                    f {
                        operationId = e.operationId;
                        election = e.ordre;
                        collegeId = e.college;
                        etablissmentId = et;
                        choix;
                        empreinteSuffrage;
                      }
                  ) filename
              )
            ) else List.iter visit children
          in
          visit e.etablissement
        ) elections
    in
    let trustees =
      string_of_file (dirname / "trustees.jsons")
      |> trustees_of_string G.read
    in
    let threshold =
      match trustees.trustees_threshold with
      | None -> failwith "missing threshold in load_tree_as_referentiel"
      | Some t -> t
    in
    let clesPubliques =
      [trustees.trustees_overall_key]
      |> List.map (fun x ->
             let open ClePubliqueBureau in
             {
               ordre = 1;
               cle_publique = string_of_unboxed G.write x;
             }
           )
    in
    let clesMembres =
      trustees.trustees_individual_keys
      |> List.mapi (fun i x ->
             let open ClePubliqueMembreBureau in
             {
               ordre_bv = 1;
               ordre_assesseur = i + 1;
               cle_publique = string_of_unboxed G.write x;
             }
           )
    in
    let dechiffrementsPartiels f =
      List.iter (fun (e : Election.t) ->
          let dirname = dirname / string_of_int e.ordre in
          let () =
            let filename = dirname / "partial_decryptions.jsons" in
            if Sys.file_exists filename then (
              iter_on_lines_of_file (fun it ->
                  let open DechiffrementPartiel in
                  f {
                      ordrebureauvote = 1;
                      seuil = threshold;
                      ordreelection = e.ordre;
                      ordreetablissement = None;
                      ordrecollege = None;
                      it;
                    }
                ) filename
            )
          in
          let rec visit et =
            let children = (IMap.find et forest).Analysis.children in
            if children = [] then (
              let dirname = dirname / string_of_int et in
              let filename = dirname / "partial_decryptions.jsons" in
              if Sys.file_exists filename then (
                iter_on_lines_of_file (fun it ->
                    let open DechiffrementPartiel in
                    f {
                        ordrebureauvote = 1;
                        seuil = threshold;
                        ordreelection = e.ordre;
                        ordreetablissement = Some et;
                        ordrecollege = Some e.college;
                        it;
                      }
                  ) filename
              )
            ) else List.iter visit children
          in
          visit e.etablissement
        ) elections
    in
    let open Referentiel in
    {
      colleges = referentiel.colleges;
      etablissements = Lazy.from_val etablissements;
      elections = Lazy.from_val elections;
      listes = referentiel.listes;
      candidats = referentiel.candidats;
      suffrages;
      clesPubliques = Lazy.from_val clesPubliques;
      clesMembres = Lazy.from_val clesMembres;
      dechiffrementsPartiels;
      clePubliqueSignatureSuffrage = referentiel.clePubliqueSignatureSuffrage;
    }

end
