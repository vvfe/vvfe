(**************************************************************************)
(*                                  VVFE                                  *)
(*                                                                        *)
(*  Copyright © 2021-2022 Inria                                           *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU Affero General Public License as        *)
(*  published by the Free Software Foundation, either version 3 of the    *)
(*  License, or (at your option) any later version, with the additional   *)
(*  exemption that compiling, linking, and/or using OpenSSL is allowed.   *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful, but   *)
(*  WITHOUT ANY WARRANTY; without even the implied warranty of            *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *)
(*  Affero General Public License for more details.                       *)
(*                                                                        *)
(*  You should have received a copy of the GNU Affero General Public      *)
(*  License along with this program.  If not, see                         *)
(*  <http://www.gnu.org/licenses/>.                                       *)
(**************************************************************************)

open Vvfe
open Signatures
open Common
open Serializable_t
open Csvparser

type 'a elections = (Election.t * 'a params * (int -> 'a params)) IMap.t

val make_elections : Referentiel.t -> 'a trustees -> 'a elections

module MakeComputeTrustees (G : GROUP) : sig
  val compute_trustees : ?seuils:int IMap.t -> Referentiel.t -> G.t trustees list
end

val compute_elections :
  Referentiel.t -> 'a trustees list ->
  (int IMap.t * int IMap.t IMap.t, ISet.t IMap.t) Either.t ->
  'a params IMap.t * 'a params IMap.t IMap.t

type etablissement_tree =
  {
    it : Etablissement.t;
    children : int list;
  }

val compute_etablissement_forest : Etablissement.t list -> int list * etablissement_tree IMap.t
val pp_etablissement_forest : Format.formatter -> int list * etablissement_tree IMap.t -> unit
