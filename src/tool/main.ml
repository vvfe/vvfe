(**************************************************************************)
(*                                  VVFE                                  *)
(*                                                                        *)
(*  Copyright © 2021-2022 Inria                                           *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU Affero General Public License as        *)
(*  published by the Free Software Foundation, either version 3 of the    *)
(*  License, or (at your option) any later version, with the additional   *)
(*  exemption that compiling, linking, and/or using OpenSSL is allowed.   *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful, but   *)
(*  WITHOUT ANY WARRANTY; without even the implied warranty of            *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *)
(*  Affero General Public License for more details.                       *)
(*                                                                        *)
(*  You should have received a copy of the GNU Affero General Public      *)
(*  License along with this program.  If not, see                         *)
(*  <http://www.gnu.org/licenses/>.                                       *)
(**************************************************************************)

open Vvfe_platform.Platform
open Vvfe
open Common
open Csvparser
open Serializable_j

module DirectMonad = struct
  type 'a t = 'a
  let return x = x
  let bind x f = f x
  let fail e = raise e
  let yield () = ()
end

module type GROUP = Vvfe_platform.Signatures.GROUP with type number := Z.t

module G = Group.Ed25519
module M = Deterministic_random
module T = Election_tree.Make (G) (M)

let ( let* ) = M.bind
let ( / ) = Filename.concat

let make_random_scalars n =
  let result = Array.make n Z.zero in
  let rec loop i =
    if i < n then (
      let* x = M.random G.q in
      result.(i) <- x;
      loop (i + 1)
    ) else M.return result
  in
  loop 0

let bench_time group n =
  let module G = (val group : GROUP) in
  Printf.eprintf "  Generating %d inputs...%!" n;
  let scalars = M.run "" (make_random_scalars n) in
  Printf.eprintf " done\n  Computing...%!";
  let t1 = Unix.gettimeofday () in
  let points = Array.map (fun x -> G.(g **~ x)) scalars in
  Printf.eprintf " done\n%!";
  let t2 = Unix.gettimeofday () in
  let _ = Array.fold_left G.( *~ ) G.one points in
  t2 -. t1

let bench_rate group n =
  let b = bench_time group n in
  1. /. (b /. float_of_int n)

let bench group n =
  let module G = (val group : GROUP) in
  Printf.eprintf "Benching %s...\n" G.description;
  let r = bench_rate group n in
  Printf.eprintf "  %.0f pow/s\n" r

let bench n =
  bench (module G) n;
  bench (module Vvfe_platform.Ed25519_pure) n;
  bench (module Vvfe_platform.Secp256r1) n

let write_file filename f xs =
  let oc = open_out filename in
  List.iter (fun x ->
      output_string oc (f x);
      output_string oc "\n"
    ) xs;
  close_out oc

let getenv var =
  match Sys.getenv_opt var with
  | None -> Printf.ksprintf failwith "%s not defined" var
  | Some i -> i

let getenv_int var =
  int_of_string @@ getenv var

let dispatch src dst =
  let operation = getenv_int "VVFE_OPERATION" in
  let tour = getenv_int "VVFE_TOUR" in
  Printf.eprintf "Loading referentiel from directory %s...\n%!" src;
  let referentiel = Referentiel.load src in
  Printf.eprintf "Dispatching to directory %s...\n%!" dst;
  Printf.eprintf "...ballots...\n%!";
  let elections = Election_tree.dispatch_ballots ~operation ~tour referentiel dst in
  Printf.eprintf "...partial decryptions...\n%!";
  match T.dispatch_partial_decryptions referentiel dst with
  | Some (seuils, elections) ->
     Printf.eprintf "...trustees...\n%!";
     let open Analysis.MakeComputeTrustees (G) in
     let trustees = compute_trustees ~seuils referentiel in
     write_file (dst / "trustees.jsons") (string_of_trustees G.write) trustees;
     Printf.eprintf "...elections...\n%!";
     let elections = Analysis.compute_elections referentiel trustees (Either.Left elections) in
     T.dispatch_elections elections dst
  | None ->
     Printf.eprintf "   (partial decryptions not found, verification will be partial)\n";
     Printf.eprintf "...trustees...\n%!";
     let open Analysis.MakeComputeTrustees (G) in
     let trustees = compute_trustees referentiel in
     write_file (dst / "trustees.jsons") (string_of_trustees G.write) trustees;
     Printf.eprintf "...elections...\n%!";
     let elections = Analysis.compute_elections referentiel trustees (Either.Right elections) in
     T.dispatch_elections elections dst


let compute_fingerprints dir =
  let tour = getenv_int "VVFE_TOUR" in
  Printf.eprintf "Computing fingerprints of %s...\n%!" dir;
  T.compute_fingerprints ~tour dir

let compute_encrypted_tally dir =
  Printf.eprintf "Computing encrypted tally of %s...\n%!" dir;
  T.compute_encrypted_tally dir

let compute_result src dir =
  let tour = getenv_int "VVFE_TOUR" in
  let referentiel = Referentiel.load src in
  Printf.eprintf "Computing result of %s...\n%!" dir;
  T.compute_result dir;
  T.generate_report tour referentiel dir

let print_etablissements dir =
  Printf.eprintf "Loading referentiel from %s...\n%!" dir;
  let referentiel = Referentiel.load dir in
  let forest = Analysis.compute_etablissement_forest @@ Lazy.force referentiel.etablissements in
  Analysis.pp_etablissement_forest Format.std_formatter forest

let init_random_tree_full seed nb_trustees threshold src dst =
  Printf.eprintf "Loading referentiel from %s...\n%!" src;
  let src = Referentiel.load src in
  let m = T.init_random_tree_full ~nb_trustees ~threshold src dst in
  Printf.eprintf "Initializing full random tree in %s...\n%!" dst;
  M.run seed m

let generate_random_tree seed nb_trustees threshold nb_ballots src dst =
  Printf.eprintf "Loading referentiel from %s...\n%!" src;
  let src = Referentiel.load src in
  let m = T.generate_random_tree ~nb_trustees ~threshold ~nb_ballots src dst in
  Printf.eprintf "Generating random tree in %s...\n%!" dst;
  M.run seed m

let generate_random_ballots seed nb_ballots dir =
  Printf.eprintf "Generating %d random ballot(s) in %s...\n%!" nb_ballots dir;
  let m = T.generate_random_ballots ~nb_ballots dir in
  let suffix = Election_tree.get_suffix dir in
  M.run (seed ^ "/ballots/" ^ suffix) m

let compute_partial_decryptions seed dir =
  Printf.eprintf "Computing partial decryption in %s...\n%!" dir;
  let m = T.compute_partial_decryptions dir in
  let suffix = Election_tree.get_suffix dir in
  M.run (seed ^ "/partial-decryptions/" ^ suffix) m

let regenerate_referentiel src dir dst =
  let tour = getenv_int "VVFE_TOUR" in
  let referentiel = Referentiel.load src in
  let referentiel = T.load_tree_as_referentiel ~tour referentiel dir in
  Referentiel.save dst referentiel

let dump_alphas src =
  Printf.eprintf "Loading referentiel from %s...\n%!" src;
  let src = Referentiel.load src in
  src.suffrages (fun x ->
      let b = ballot_of_string G.read x.choix in
      Array.iter (fun x ->
          Array.iter (fun x ->
              print_endline (G.to_string x.Question_h_t.cipherText.alpha)
            ) x.Question_h_t.choices
        ) b.answers
    )

let dump_tokenids src =
  Printf.eprintf "Loading referentiel from %s...\n%!" src;
  let src = Referentiel.load src in
  src.suffrages (fun x ->
      let b = ballot_of_string G.read x.choix in
      print_endline b.tokenId
    )

let dispatch_results src dst =
  Printf.eprintf "Dispatching results from %s to %s...\n%!" src dst;
  Election_tree.dispatch_results src dst

let slurp_stdin () =
  let rec loop accu =
    match read_line () with
    | exception End_of_file -> String.concat "" (List.rev accu)
    | line -> loop (line :: accu)
  in
  loop []

let check_reference dir =
  let operation = getenv_int "VVFE_OPERATION" in
  let tour = getenv_int "VVFE_TOUR" in
  let rootfp = getenv "VVFE_ROOTFP" in
  let module L =
    struct
      let load path =
        let file =
          match path with
          | `Root -> "fingerprints-root.json"
          | `Index i -> string_of_int i / "fingerprints-index.json"
          | `File (i, k) -> string_of_int i / Printf.sprintf "fingerprints-%s.txt" k
        in
        string_of_file (dir / file)
    end
  in
  let module X = Individual.Make (DirectMonad) (L) in
  let reference = Individual.normalize_reference (slurp_stdin ()) in
  let b = X.check_reference ~rootfp ~operation ~tour reference in
  match b with
  | Ok (h, i) ->
     Printf.printf "%s%s\n" h (if i then " (invalid)" else "");
     exit 0
  | Error e ->
     let msg =
       match e with
       | `Missing -> "Référence non trouvée"
       | `Syntax msg -> Printf.sprintf "Erreur de syntaxe (%s)" msg
       | `Server msg -> Printf.sprintf "Erreur du serveur (%s)" msg
     in
     Printf.eprintf "%s\n" msg;
     exit 2

let check_cachet pem =
  let operation = getenv_int "VVFE_OPERATION" in
  let tour = getenv_int "VVFE_TOUR" in
  let pubkey = load_cachet_pubkey pem in
  let cachet = Individual.normalize_cachet (slurp_stdin ()) in
  let b = Individual.check_cachet ~operation ~tour ~pubkey cachet in
  match b with
  | Ok (_, _, reference, b) ->
     print_endline reference;
     exit (if b () then 0 else 1)
  | Error msg ->
     Printf.eprintf "Erreur: %s\n%!" msg;
     exit 2

let split_long_line line_length x =
  let n = String.length x in
  let buf = Buffer.create n in
  let rec loop i =
    if i + line_length < n then (
      Buffer.add_substring buf x i line_length;
      Buffer.add_char buf '\n';
      loop (i + line_length)
    ) else (
      Buffer.add_substring buf x i (n - i);
      Buffer.contents buf
    )
  in
  loop 0

let generate_recepisse seed election etablissement dir =
  let operation = getenv_int "VVFE_OPERATION" in
  let tour = getenv_int "VVFE_TOUR" in
  let referentiel = Referentiel.load dir in
  let ballot = slurp_stdin () in
  let h =
    Printf.ksprintf sha256_hex "%s%d%d%d%d"
      ballot operation tour election etablissement
  in
  let cle = get_rib_key h |> Printf.sprintf "%02d" in
  let reference =
    Printf.sprintf "%d%d&%d&%s%s" operation tour election h cle
  in
  let cachet =
    let privkey = getenv "VVFE_CACHET_PRIVKEY" in
    let module G = Vvfe_platform.Secp256r1 in
    let module S = Schnorr.Make (M) (G) in
    let privkey = Z.(of_hex privkey mod G.q) in
    let pubkey = G.(g **~ privkey) in
    if G.(compare pubkey (of_string @@ Lazy.force referentiel.clePubliqueSignatureSuffrage) <> 0) then
      Printf.eprintf "Attention : la clé publique de signature de suffrage ne correspond pas à VVFE_CACHET_PRIVKEY !\n";
    let keypair = privkey, pubkey in
    let infoSU =
      Printf.sprintf "%d%d|%d||%d|%s|%s" operation tour election etablissement h cle
    in
    let publicKeySu =
      Printf.sprintf
        "-----BEGIN_VERIFICATION_KEY-----\r\n%s\r\n-----END_VERIFICATION_KEY-----"
        G.(to_string pubkey)
    in
    let msg = sha256_hex infoSU in
    let schnorr = M.run seed (S.create_signature keypair ~msg) in
    let cachetBrutSU = infoSU ^ schnorr ^ replace_underscores_by_spaces publicKeySu in
    let cleCachetBrut = get_rib_key cachetBrutSU |> Printf.sprintf "%02d" in
    {infoSU; schnorr; publicKeySu; cleCachetBrut}
    |> string_of_cachet
    |> encode_base64
    |> split_long_line 76
  in
  Printf.printf "Référence du bulletin:\n\n%s\n\n" reference;
  Printf.printf "Cachet:\n\n%s\n" cachet

let compute_election_map src =
  Printf.eprintf "Loading referentiel from %s...\n%!" src;
  let src = Referentiel.load src in
  let elections =
    Lazy.force src.elections
    |> List.map (fun (x : Election.t) -> string_of_int x.ordre, `String x.nom)
    |> (fun x -> `Assoc x)
  in
  let etablissements =
    Lazy.force src.etablissements
    |> List.map (fun (x : Etablissement.t) -> string_of_int x.ordre, `String x.nom)
    |> (fun x -> `Assoc x)
  in
  `Assoc ["elections", elections; "etablissements", etablissements]
  |> Yojson.Safe.to_string
  |> print_string

let generate_iverif_init_js ~rootfp ~operation ~tour ~pubkey ~election_map =
  Printf.sprintf
{js|"use strict";
var vvfe_rootfp = "%s";
var vvfe_operation = %d;
var vvfe_tour = %d;
var vvfe_cachet_pk = "%s";
var vvfe_election_map = %s;
|js} rootfp operation tour pubkey election_map

let generate_iverif_index_html ~initfp ~mainfp =
  let open Tyxml.Html in
  html
    ~a:[
      a_xmlns `W3_org_1999_xhtml;
      a_dir `Ltr;
      a_lang "fr";
    ]
    (head
       (title (txt "VVFE"))
       [
         meta ~a:[
             a_http_equiv "Content-Type";
             a_content "text/html;charset=utf-8";
           ] ();
         link ~rel:[`Stylesheet] ~href:("style.css") ();
         script ~a:[
             a_src "init.js";
             Printf.ksprintf a_integrity "sha256-%s" initfp;
             a_crossorigin `Anonymous;
           ] (txt "");
         script ~a:[
             a_src "iverif.js";
             Printf.ksprintf a_integrity "sha256-%s" mainfp;
             a_crossorigin `Anonymous;
           ] (txt "");
       ]
    )
    (body [])

let populate_web_iverif election_map share pem dst =
  let operation = getenv_int "VVFE_OPERATION" in
  let tour = getenv_int "VVFE_TOUR" in
  let pubkey = load_cachet_pubkey pem in
  let election_map = string_of_file election_map in
  let rootfp =
    let x = string_of_file (dst / "fingerprints-root.json") in
    sha256_hex x
  in
  let initfp =
    let x = generate_iverif_init_js ~rootfp ~operation ~tour ~pubkey ~election_map in
    string_to_file ~filename:(dst / "init.js") x;
    sha256_b64 x
  in
  let mainfp =
    let x = string_of_file (share / "iverif.js") in
    string_to_file ~filename:(dst / "iverif.js") x;
    sha256_b64 x
  in
  let () =
    List.iter (fun name ->
        let x = string_of_file (share / name) in
        string_to_file ~filename:(dst / name) x
      ) ["style.css"; "seal.png"; "aide.png"]
  in
  let html = generate_iverif_index_html ~initfp ~mainfp in
  let oc = open_out (dst / "index.html") in
  let ocf = Format.formatter_of_out_channel oc in
  Format.fprintf ocf "%a\n%!" (Tyxml.Html.pp ()) html;
  close_out oc

let generate_cachet_init_js ~operation ~tour ~pubkey ~election_map =
  Printf.sprintf
{js|"use strict";
var vvfe_operation = %d;
var vvfe_tour = %d;
var vvfe_cachet_pk = "%s";
var vvfe_election_map = %s;
|js} operation tour pubkey election_map

let generate_cachet_index_html ~initfp ~mainfp =
  let open Tyxml.Html in
  html
    ~a:[
      a_xmlns `W3_org_1999_xhtml;
      a_dir `Ltr;
      a_lang "fr";
    ]
    (head
       (title (txt "VVFE"))
       [
         meta ~a:[
             a_http_equiv "Content-Type";
             a_content "text/html;charset=utf-8";
           ] ();
         link ~rel:[`Stylesheet] ~href:("style.css") ();
         script ~a:[
             a_src "init.js";
             Printf.ksprintf a_integrity "sha256-%s" initfp;
             a_crossorigin `Anonymous;
           ] (txt "");
         script ~a:[
             a_src "cachet.js";
             Printf.ksprintf a_integrity "sha256-%s" mainfp;
             a_crossorigin `Anonymous;
           ] (txt "");
       ]
    )
    (body [])

let populate_web_cachet election_map share pem dst =
  let operation = getenv_int "VVFE_OPERATION" in
  let tour = getenv_int "VVFE_TOUR" in
  let pubkey = load_cachet_pubkey pem in
  let election_map = string_of_file election_map in
  let initfp =
    let x = generate_cachet_init_js ~operation ~tour ~pubkey ~election_map in
    string_to_file ~filename:(dst / "init.js") x;
    sha256_b64 x
  in
  let mainfp =
    let x = string_of_file (share / "cachet.js") in
    string_to_file ~filename:(dst / "cachet.js") x;
    sha256_b64 x
  in
  let () =
    List.iter (fun name ->
        let x = string_of_file (share / name) in
        string_to_file ~filename:(dst / name) x
      ) ["style.css"; "seal.png"; "aide.png"]
  in
  let html = generate_cachet_index_html ~initfp ~mainfp in
  let oc = open_out (dst / "index.html") in
  let ocf = Format.formatter_of_out_channel oc in
  Format.fprintf ocf "%a\n%!" (Tyxml.Html.pp ()) html;
  close_out oc

type _ arg =
  | String : string arg
  | Int : int arg

type 'a labelled_arg =
  {
    label : string;
    arg : 'a arg;
    cast : string -> 'a;
  }

type _ arg_list =
  | Nil : unit arg_list
  | Cons : 'a labelled_arg * 'b arg_list -> ('a -> 'b) arg_list

type ex_list = L : 'a arg_list -> ex_list

type arg_handler =
  | H : 'a arg_list * 'a -> arg_handler

let string label = {label; arg = String; cast = (fun x -> x)}
let int label = {label; arg = Int; cast = int_of_string}
let unit = Nil
let ( ** ) a b = Cons (a, b)

let prerr_spec (cmd, H (spec, _)) =
  Printf.eprintf "  %s" cmd;
  let rec loop (L spec) =
    match spec with
    | Nil -> ()
    | Cons (a, b) ->
       let t = match a.arg with String -> "string" | Int -> "int" in
       Printf.eprintf " <%s:%s>" a.label t;
       loop (L b)
  in
  loop (L spec);
  Printf.eprintf "\n"

let usage spec =
  Printf.eprintf "Usage:\n\n  vvfe-tool <command>\n\nwhere <command> can be:\n\n";
  List.iter prerr_spec spec;
  exit 1

let rec eval (H (spec, f)) args =
  match spec with
  | Nil -> true
  | Cons (x, xs) ->
     match args with
     | [] -> false
     | arg :: args -> eval (H (xs, f (x.cast arg))) args

let process_args spec args =
  match args with
  | [] -> usage spec
  | cmd :: args ->
     match List.assoc_opt cmd spec with
     | Some h when eval h args -> ()
     | _ -> usage spec

let dir = string "dir" ** unit
let dst = string "dst" ** unit
let src_dst = string "src" ** dst
let share_pem_dst = string "share" ** string "pem" ** dst
let populate = string "election_map" ** share_pem_dst

let args_spec =
  [
    "bench", H (int "n" ** unit, bench);
    "dump-alphas", H (dir, dump_alphas);
    "dump-tokenids", H (dir, dump_tokenids);
    "dispatch", H (src_dst, dispatch);
    "compute-fingerprints", H (dir, compute_fingerprints);
    "compute-encrypted-tally", H (dir, compute_encrypted_tally);
    "compute-result", H (src_dst, compute_result);
    "print-etablissements", H (dir, print_etablissements);
    "init-random-tree-full", H (string "seed" ** int "nb_trustees" ** int "threshold" ** src_dst, init_random_tree_full);
    "generate-random-tree", H (string "seed" ** int "nb_trustees" ** int "threshold" ** int "nb_ballots" ** src_dst, generate_random_tree);
    "generate-random-ballots", H (string "seed" ** int "nb_ballots" ** dir, generate_random_ballots);
    "compute-partial-decryptions", H (string "seed" ** dir, compute_partial_decryptions);
    "dispatch-results", H (src_dst, dispatch_results);
    "regenerate-referentiel", H (string "src" ** string "dir" ** dst, regenerate_referentiel);
    "check-reference", H (dir, check_reference);
    "check-cachet", H (string "pem" ** unit, check_cachet);
    "generate-recepisse", H (string "seed" ** int "election" ** int "etablissement" ** dir, generate_recepisse);
    "compute-election-map", H (dir, compute_election_map);
    "populate-web-iverif", H (populate, populate_web_iverif);
    "populate-web-cachet", H (populate, populate_web_cachet);
  ]

let main = process_args args_spec

let () =
  if not !Sys.interactive then (
    match Array.to_list Sys.argv with
    | [] -> failwith "no argv"
    | _ :: args -> main args
  )
