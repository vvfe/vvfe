#!/bin/sh

set -e

VVFE_SRC="${VVFE_SRC:-$PWD}"

export VVFE_SYSROOT="${VVFE_SYSROOT:-$HOME/.vvfe}"
export OPAMROOT="$VVFE_SYSROOT/opam"
export XDG_CACHE_HOME="$VVFE_SYSROOT/cache"

if [ -e "$VVFE_SYSROOT" ]; then
    echo "$VVFE_SYSROOT already exists."
    echo "Please remove it or set VVFE_SYSROOT to a non-existent directory first."
    exit 1
fi

mkdir -p "$VVFE_SYSROOT"
cd "$VVFE_SYSROOT"

echo
echo "=-=-= Cloning OPAM repository =-=-="
echo
mkdir opam-repository
cd opam-repository
git init
git remote add origin https://github.com/ocaml/opam-repository.git
git fetch --depth=1 origin 452dafb86d0581a04c80622b5854a8ee1203961a:opam
git checkout opam

if [ -z "$VVFE_USE_SYSTEM_OPAM" ]; then

    # Download and install opam

    # Check that Dune is not installed
    # cf. https://github.com/ocaml/opam/issues/3987
    if command -v dune >/dev/null; then
        echo "Please uninstall Dune first, or remove it from your PATH."
        exit 1
    fi

    echo
    echo "=-=-= Download and check tarballs =-=-="
    echo

    # Look for wget or curl
    if which wget >/dev/null; then
        echo "wget was found and will be used"
    elif which curl >/dev/null; then
        wget () { curl "$1" > "${1##*/}"; }
        echo "curl was found and will be used"
    fi

    mkdir -p "$VVFE_SYSROOT/bootstrap/src"

    cd "$VVFE_SYSROOT/bootstrap/src"
    wget https://github.com/ocaml/opam/releases/download/2.1.4/opam-full-2.1.4.tar.gz

    if which sha256sum >/dev/null; then
        sha256sum --check <<EOF
1643609f4eea758eb899dc8df57b88438e525d91592056f135e6e045d0d916cb  opam-full-2.1.4.tar.gz
EOF
    else
        echo "WARNING: sha256sum was not found, checking tarballs is impossible!"
        exit 2
    fi

    export PATH="$VVFE_SYSROOT/bootstrap/bin:$PATH"

    echo
    echo "=-=-= Compilation and installation of OPAM =-=-="
    echo
    cd "$VVFE_SYSROOT/bootstrap/src"
    tar -xzf opam-full-2.1.4.tar.gz
    cd opam-full-2.1.4
    make cold CONFIGURE_ARGS="--prefix $VVFE_SYSROOT/bootstrap"
    make cold-install LIBINSTALL_DIR="$VVFE_SYSROOT/bootstrap/lib/ocaml"

    cat > $VVFE_SYSROOT/env.sh <<EOF
PATH="$VVFE_SYSROOT/bootstrap/bin:\$PATH"; export PATH;
EOF

fi

echo
echo "=-=-= Generation of env.sh =-=-="
echo
cat >> $VVFE_SYSROOT/env.sh <<EOF
OPAMROOT=$OPAMROOT; export OPAMROOT;
XDG_CACHE_HOME=$XDG_CACHE_HOME; export XDG_CACHE_HOME;
eval \$(opam env)
EOF
ln -sf $VVFE_SYSROOT/env.sh $VVFE_SRC/env.sh

echo
echo "=-=-= Initialization of OPAM root =-=-="
echo
opam init $VVFE_OPAM_INIT_ARGS --bare --no-setup -k git "$VVFE_SYSROOT/opam-repository"
opam switch create 4.13.1 ocaml-base-compiler.4.13.1
eval $(opam env)

echo
echo "=-=-= Installation of Vvfe build-dependencies =-=-="
echo
opam install --yes dune yojson atdgen zarith cryptokit csv uutf uucp camomile tyxml js_of_ocaml js_of_ocaml-ppx js_of_ocaml-lwt js_of_ocaml-tyxml

echo
echo "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-="
echo
echo "Vvfe build-dependencies have been successfully compiled and installed"
echo "to $VVFE_SYSROOT. The directory"
echo "  $VVFE_SYSROOT/bootstrap/src"
echo "can be safely removed now."
echo
echo "Next, you need to run the following commands or add them to your ~/.bashrc"
echo "or equivalent:"
echo "  source $VVFE_SRC/env.sh"
echo "Note that if you use a Bourne-incompatible shell (e.g. tcsh), you'll have"
echo "to adapt env.sh to your shell."
echo
echo "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-="
echo
