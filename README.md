Vvfe
====


Introduction
------------

Vvfe is a verifier for the 2022 French legislative elections for
French people living abroad. It is derived from
[Belenios](https://www.belenios.org). It is targeted at a French
audience, hence most of its [documentation](LISEZMOI.md) is in French.

Compilation instructions are provided in [INSTALL.md](INSTALL.md).


Legal
-----

### Internal code

By "internal code", we mean everything that is not in the `ext/`
directory.

Copyright © 2012-2022 Inria, CNRS

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version, with the additional
exemption that compiling, linking, and/or using OpenSSL is allowed.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Affero General Public License for more details.

### External code

Please refer to each file for accurate copyright and licensing
information.
