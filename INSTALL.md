Vvfe compilation instructions
=============================

Vvfe is written in OCaml and has some dependencies towards third-party
OCaml libraries. The easiest and most portable way to compile Vvfe
from source is to use [OPAM](http://opam.ocamlpro.com/), which is a
package manager for OCaml projects.

The non-OCaml prerequisites are:

 * a POSIX system with a C compiler
 * on Linux, [Bubblewrap](https://github.com/projectatomic/bubblewrap)
 * [GMP](http://gmplib.org/)
 * [pkg-config](http://www.freedesktop.org/wiki/Software/pkg-config/)
 * [Wget](https://www.gnu.org/software/wget/) or [curl](http://curl.haxx.se/)
 * [ncurses](http://invisible-island.net/ncurses/)
 * [zlib](http://zlib.net/)
 * [libsodium](https://www.libsodium.org/)
 * `parallel` from [moreutils](https://joeyh.name/code/moreutils/)

These libraries and tools are pretty common, and might be directly part
of your operating system. On [Debian](http://www.debian.org/) and its
derivatives, they can be installed with the following command:

    sudo apt install bubblewrap build-essential libgmp-dev pkg-config wget ca-certificates libncurses-dev zlib1g-dev libsodium-dev moreutils

If you are unfamiliar with OCaml or OPAM, we provide an
`opam-bootstrap.sh` shell script that creates a whole, hopefully
self-contained, OCaml+OPAM install, and then installs all the
dependencies of Vvfe, everything into a single directory. You can
choose the directory by setting the `VVFE_SYSROOT` environment
variable, or it will take `~/.vvfe` by default. Just run:

    ./opam-bootstrap.sh

On a modern desktop system, this needs approximately 10 minutes and 2.8
gigabytes of disk space.

Alternativement, if you already have OPAM installed, you can directly
execute the `opam install` command of `opam-bootstrap.sh`.

If everything goes successfully, follow the given instructions to
update your shell environment, then run:

    make all


Troubleshooting
---------------

### Bootstrap fails if dune is already installed

The script `opam-bootstrap.sh` fails when a not suitable version of
dune is already installed in your `$PATH`. This is due to [a bug in
opam](https://github.com/ocaml/opam/issues/3987). If you face this
issue, either uninstall dune before running `opam-bootstrap.sh`, or
manage to get opam running by other means, and directly use it to
install the dependencies of Vvfe.

### Bootstrap fails because of an error with an OPAM package

For reproducibility purposes, the `opam-bootstrap.sh` script hardcodes
a specific revision of the OPAM repository. However, it may happen
that this revision becomes unusable, e.g. the URL of some tarball
changes. This may give errors like bad checksums when running the
script.

To recover from such errors, update your local copy of the OPAM
repository with the following commands:

    source env.sh
    cd $OPAMROOT/../opam-repository
    git pull --ff-only
    opam update

then run the `opam install` command that can be found in the
`opam-bootstrap.sh` script.

### Missing sources

The instructions outlined in this document and in the
`opam-bootstrap.sh` script imply downloading files from third-party
servers. Sometimes, these servers can be down. For example, you can
get:

    =-=-= Installing ocamlnet.3.7.3 =-=-=
    ocamlnet.3.7.3 Downloading http://download.camlcity.org/download/ocamlnet-3.7.3.tar.gz
    [ERROR] http://download.camlcity.org/download/ocamlnet-3.7.3.tar.gz is not available
    
    ===== ERROR while installing ocamlnet.3.7.3 =====
    Could not get the source for ocamlnet.3.7.3.

This can be worked around with the following steps:

 * source the generated `env.sh` file (you must adapt it if you use an
   incompatible shell such as tcsh);
 * download the file from an alternate source (for example
   [Debian source packages](http://www.debian.org/distrib/packages));
 * run `opam pin <package-name> <path-to-file-download-above>` (in the
   example above, `<package-name>` would be `ocamlnet`);
 * resume the installation by running again the `opam install` command
   found in `opam-bootstrap.sh`;
 * follow the instructions given at the end of `opam-bootstrap.sh`.

### Errors while compiling Vvfe itself

If you succeeded installing all dependencies, but you get errors while
compiling Vvfe, maybe you installed an incompatible version of a
dependency. The `opam-bootstrap.sh` script is tuned to install only
compatible versions; you can have a look at it to get these versions.
