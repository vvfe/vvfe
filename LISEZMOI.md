Vvfe: Vérifiabilité du Vote des Français de l'Étranger
======================================================


Introduction
------------

Vvfe est un vérificateur pour les élections législatives 2022 des
Français de l'étranger. Il est dérivé de
[Belenios](https://www.belenios.org).

Il a été commandé par le MEAE (Ministère de l'Europe et des Affaires
Étrangères), organisateur de ces élections, et réalisé en suivant des
spécifications et jeux de données fournies par Voxaly, prestataire du
MEAE.

Des instructions de compilation (en anglais) sont fournies dans
[INSTALL.md](INSTALL.md). La compilation produit l'outil de
vérification sous la forme d'un exécutable `vvfe-tool` dans
`_build/install/default/bin`.

L'outil `vvfe-tool` possède plusieurs sous-commandes dont le détail et
la syntaxe peuvent être trouvés dans le [code
source](src/tool/main.ml). Une opération de vérification consiste à
exécuter plusieurs de ces sous-commandes, dans un ordre précis. Un
[script shell](run.sh) est fourni pour orchestrer une vérification
complète. Il prend un unique argument, le chemin vers le répertoire
des données de l'élection à vérifier (un ensemble de fichiers CSV).


Structure des données et terminologie
-------------------------------------

Il y a 1 _bureau de vote_, qui regroupe plusieurs _membres_ ayant
chacun une paire de clés (privée et publique, seule la clé publique
est fournie). Le bureau de vote possède lui-même une clé publique
globale (calculée à partir des clés publiques des membres) qui est
utilisée pour chiffrer les bulletins.

Il y a 11 _élections_, une par circonscription législative. Chaque
circonscription est divisée en _établissements_, organisés dans une
structure arborescente, qui peut être visualisée avec la commande
suivante :

    vvfe-tool print-etablissements $DIR

Nous appelons _urne atomique_ un ensemble de _bulletins_ (encore
appelées _suffrages_) pour une élection et un établissement donnés. Il
y a un déchiffrement partiel par urne atomique (et par membre du
bureau de vote).

Nous appelons _urne agglomérée_ l'ensemble des bulletins de tous les
établissements d'une élection donnée. Il y a un déchiffrement partiel
par urne agglomérée (et par membre du bureau de vote).


Flux opérationnel
-----------------

La vérification se fait en plusieurs étapes. Dans ce qui suit, `$SRC`
désigne le répertoire des données d'élection (fichiers CSV), `$DST`
désigne un répertoire qui est typiquement temporaire et `$DIR` désigne
un sous-répertoire de `$DST`.

Les variables d'environnement `VVFE_TOUR` et `VVFE_OPERATION` sont
nécessaires pour effectuer les opérations de vérification. Par
exemple :

    export VVFE_TOUR=1
    export VVFE_OPERATION=8001

La variable `VVFE_TOUR` indique s'il s'agit du premier ou du second tour.
La variable `VVFE_OPERATION` est un nombre entier, que l'on retrouve en
début de chaque ligne dans la liste des bulletins chiffrés.

Ensuite, on peut lancer la commande :

    ./run.sh $SRC

qui effectuera toutes les opérations à la suite. Nous détaillons
maintenant chacune des étapes effectuées par ce script.

### Analyse des CSV et tri des données

La commande suivante :

    vvfe-tool dispatch $SRC $DST

effectue une première analyse des données dans `$SRC`, et répartit les
bulletins et les déchiffrements partiels dans des sous-répertoires
différents de `$DST` (qui doit être initialement un répertoire vide),
par élection et par établissement. Elle génère également des fichiers
annexes utiles pour la vérification de chaque urne.

## Calcul des résultats chiffrés et des empreintes

Les commandes suivantes :

    vvfe-tool compute-encrypted-tally $DIR
    vvfe-tool compute-fingerprints $DIR

vont vérifier tous les bulletins de `$DIR`, et en calculer la combinaison
homomorphique (encore appelée « accumulation »), ainsi que des empreintes
utiles pour la suite. Elles doivent être exécutées sur chaque urne, et
d'abord sur chaque urne atomique avant l'urne agglomérée correspondante.

## Déchiffrement des résultats

La commande suivante :

    vvfe-tool compute-result $SRC $DIR

va vérifier et combiner les déchiffrements partiels de `$DIR` pour
calculer le résultat en clair. Elle génère un procès-verbal
`result.md`.


Mentions légales
----------------

### Code interne

Par « code interne », nous parlons de tout ce qui n'est pas dans le
répertoire `ext/`.

Copyright © 2012-2022 Inria, CNRS

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version, with the additional
exemption that compiling, linking, and/or using OpenSSL is allowed.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Affero General Public License for more details.

### Code externe

Veuillez consulter chaque fichier pour obtenir des informations
précises sur les copyrights et licences.
